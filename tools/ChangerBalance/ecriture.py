# -*- coding:utf-8 -*-

import sys
import os
import ctypes

# Ajouter common comme PATH pour accéder à DAO et DTO
os.getcwd()
sys.path.append(os.getcwd() + "\\common")

from internal.DAO.daoBalanceFactory import *
from internal.DAO.daoBalanceCSV import *
from internal.DAO.daoBalanceOracle import *

# Création des DAO Oracle et CSV
factory = DAOBalanceFactory()
daoOracle = factory.create("oracle")
daoCSV = factory.create("csv")

# Lecture à partir du CSV
dtoBalance = daoCSV.read(daoCSV.chooseFile())

# Afficher les erreurs de balances
if dtoBalance != None:
	if len(dtoBalance.erreurs) > 0:
		strErreur = "Des erreurs ont été détectées dans la balance :\n\n"
		for erreur in dtoBalance.erreurs:
			strErreur += erreur + "\n"
		ctypes.windll.user32.MessageBoxW(0, unicode(strErreur, "utf-8"), u"Erreur de balance", 1)

	# afficher le résultat d'écriture (erreur ou succès)
	connectionError, commandError = daoOracle.update(dtoBalance)
	if connectionError:
		ctypes.windll.user32.MessageBoxW(0, u"Impossible de se connecter à la base de données.", u"Erreur de connexion", 1)
	elif commandError:
		ctypes.windll.user32.MessageBoxW(0, u"L'écriture dans la base de données est impossible. Voir le code d'erreur dans la console.", u"Erreur d'écriture", 1)
	else:
		ctypes.windll.user32.MessageBoxW(0, u"La nouvelle balance à été écrite à la base de données.", u"Succès", 1)