# -*- coding:utf-8 -*-

import sys
import os
import ctypes

# Ajouter common comme PATH pour accéder à DAO et DTO


sys.path.append(os.getcwd() + "\\common")

from internal.DAO.daoBalanceFactory import *
from internal.DAO.daoBalanceCSV import *
from internal.DAO.daoBalanceOracle import *
from internal.DTO.sanitizer import *

# Création des DAO Oracle et CSV
factory = DAOBalanceFactory()
daoOracle = factory.create("oracle")
daoCSV = factory.create("csv")

# Lecture à partir de Oracle et nettoyage
dtoBalances = daoOracle.read()
sanitizer = Sanitizer()
dtoBalance = sanitizer.sanitizeClean(dtoBalances[0], dtoBalances[1], dtoBalances[2])
dtoBalances = (dtoBalances[0], dtoBalances[1], dtoBalance)

# Afficher le résultat de la lecture (erreur ou succès)
if len(dtoBalance.erreurs) > 0:
	strErreur = "Des erreurs ont été détectées :\n\n"
	for erreur in dtoBalance.erreurs:
		strErreur += erreur + "\n"
	ctypes.windll.user32.MessageBoxW(0, unicode(strErreur, "utf-8"), u"Erreur de lecture", 1)


# Choisir l'emplacement du CSV et sauvegarder
daoCSV.chooseFile(create = True)
daoCSV.update(dtoBalances)

# Démarrer Excel
if daoCSV.fileName != "":
	os.system('start excel.exe "%s"' % daoCSV.fileName)