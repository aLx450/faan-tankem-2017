-- IMPORTANT : Assurez-vous d'avoir exécuté le script dropAllTables.cmd avant n'importe quel autre de cette liste

-- Creation des tables
CREATE TABLE Games(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	level_name VARCHAR2(20) NOT NULL,
	player1 NUMBER NOT NULL,
	player2 NUMBER NOT NULL,
    winner NUMBER NOT NULL,
	CONSTRAINT fk_Games_Levels FOREIGN KEY (level_name) REFERENCES Levels(name),
	CONSTRAINT fk_player1_Games_Users FOREIGN KEY (player1) REFERENCES Users(id),
	CONSTRAINT fk_player2_Users FOREIGN KEY (player2) REFERENCES Users(id),
	CONSTRAINT fk_Games_Users FOREIGN KEY (winner) REFERENCES Users(id)
);

CREATE TABLE Weapons(
	name VARCHAR2(50) PRIMARY KEY
);

CREATE TABLE Shooting_Stats(
	game_id NUMBER NOT NULL,
	user_id NUMBER NOT NULL,
	weapon_name VARCHAR2(50) NOT NULL,
	nb_shots NUMBER NOT NULL,
	CONSTRAINT pk_Shooting_Stats PRIMARY KEY (game_id, user_id, weapon_name),
	CONSTRAINT fk_Shooting_Stats_Games FOREIGN KEY (game_id) REFERENCES Games(id),
	CONSTRAINT fk_Shooting_Stats_Users FOREIGN KEY (user_id) REFERENCES Users(id),
	CONSTRAINT fk_Shooting_Stats_Weapons FOREIGN KEY (weapon_name) REFERENCES Weapons(name)
);