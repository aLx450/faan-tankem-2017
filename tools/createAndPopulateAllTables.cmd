REM IMPORTANT : Idéalement, exécutez le script dropAllTables.cmd avant celui-ci

chcp 65001

echo "Création des tables de balance et données associées"
echo | sqlplus e1461027/FAANb62@\"10.57.4.60/DECINFO.edu\" @creationTableBalance.sql
echo "Opération réussie"
timeout 1

echo "Création des tables de niveaux et données associées"
echo | sqlplus e1461027/FAANb62@\"10.57.4.60/DECINFO.edu\" @creationTableNiveaux.sql
echo "Opération réussie"
timeout 1

echo "Création des tables d'usagers et données associées"
echo | sqlplus e1461027/FAANb62@\"10.57.4.60/DECINFO.edu\" @creationTableUtilisateurs.sql
echo "Opération réussie"
timeout 1

echo "Création des tables de sauvegarde et données associées"
echo | sqlplus e1461027/FAANb62@\"10.57.4.60/DECINFO.edu\" @creationTableSauvegarde.sql
echo "Opération réussie"
timeout 1

echo "Création des tables de statistiques de parties et données associées"
echo | sqlplus e1461027/FAANb62@\"10.57.4.60/DECINFO.edu\" @creationTableStatsPartie.sql
echo "Opération réussie"
timeout 1

echo "Toutes les tables ont été créées et populées adéquatement"
pause