-- IMPORTANT : Assurez-vous d'avoir exécuté le script dropAllTables.cmd avant n'importe quel autre de cette liste

-- Creation des tables
CREATE TABLE Levels(
	name VARCHAR2(20) NOT NULL PRIMARY KEY,
    status VARCHAR2(7) NOT NULL,
	width NUMBER NOT NULL,
	height NUMBER NOT NULL,
	min_apparition_delay NUMBER NOT NULL,
    max_apparition_delay NUMBER NOT NULL,
	creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT chk_status CHECK (status IN ('Actif', 'Test', 'Inactif'))
);

CREATE TABLE Tiles(
	level_name VARCHAR2(20) NOT NULL,
	pos_x NUMBER NOT NULL,
	pos_y NUMBER NOT NULL,
	tile_type VARCHAR2(20),
	with_tree CHAR NOT NULL,
	CONSTRAINT pk_Tiles PRIMARY KEY (level_name, pos_x, pos_y),
	CONSTRAINT fk_Tiles_Levels FOREIGN KEY (level_name) REFERENCES Levels(name),
	CONSTRAINT chk_with_tree CHECK (with_tree IN ('Y', 'N')),
	CONSTRAINT chk_tile_type CHECK (tile_type IN ('Floor', 'FixedWall', 'AnimatedWallUp', 'AnimatedWallDown')),
	CONSTRAINT chk_pos_x_Tiles CHECK (pos_x >= 0 AND pos_x <= 11),
	CONSTRAINT chk_pos_y_Tiles CHECK (pos_y >= 0 AND pos_y <= 11)
);

CREATE TABLE Players_position(
	level_name VARCHAR2(20) NOT NULL,
	player_no NUMBER NOT NULL,
	pos_x NUMBER NOT NULL,
	pos_y NUMBER NOT NULL,
	CONSTRAINT pk_Players_position PRIMARY KEY (level_name, player_no),
	CONSTRAINT fk_Players_position_Levels FOREIGN KEY (level_name) REFERENCES Levels(name),
	CONSTRAINT chk_pos_x_Players_position CHECK (pos_x >= 0 AND pos_x <= 11),
	CONSTRAINT chk_pos_y_Players_position CHECK (pos_y >= 0 AND pos_y <= 11)
);


-- Insertions dans Levels
INSERT INTO Levels (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY) 
			VALUES ('Goalie', 'Test', 8, 8, 3, 6);
INSERT INTO Levels (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY) 
			VALUES ('Containers', 'Actif', 9, 9, 3, 6);
INSERT INTO Levels (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY) 
			VALUES ('Camper', 'Test', 9, 9, 3, 6);
INSERT INTO Levels (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY) 
			VALUES ('Hey Faan', 'Actif', 12, 12, 3, 7);
INSERT INTO Levels (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY) 
			VALUES ('Aleatoire', 'Inactif', 10, 10, 3, 7);

-- Insertions dans Tiles
-- Goalie
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 0, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 1, 1, 'AnimatedWallUp', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 2, 2, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 3, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 4, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 5, 2, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 6, 1, 'AnimatedWallUp', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 7, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 0, 7, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 1, 6, 'AnimatedWallDown', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 2, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 3, 4, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 4, 4, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 5, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 6, 6, 'AnimatedWallDown', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Goalie', 7, 7, 'AnimatedWallDown', 'N');

-- Containers
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 1, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 2, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 0, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 1, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 2, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 1, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 2, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 3, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 2, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 1, 'AnimatedWallUp', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 8, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 7, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 6, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 5, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 7, 5, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 6, 5, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 5, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 6, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 7, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 8, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 6, 8, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 7, 8, 'AnimatedWallUp', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 0, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 1, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 2, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 3, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 7, 3, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 6, 3, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 3, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 2, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 1, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 0, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 6, 0, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 7, 0, 'AnimatedWallDown', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 8, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 7, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 6, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 1, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 2, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 5, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 6, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 7, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 8, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 2, 8, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 1, 8, 'AnimatedWallDown', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 0, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 1, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 2, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 3, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 4, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 5, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 6, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 7, 4, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 8, 4, 'FixedWall', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 4, 1, 'Floor', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 4, 3, 'Floor', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 4, 5, 'Floor', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Containers', 4, 7, 'Floor', 'Y');

-- Camper
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 0, 0, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 1, 0, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 2, 0, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 3, 0, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 0, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 0, 1, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 1, 1, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 2, 1, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 3, 1, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 1, 'FixedWall', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 0, 2, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 1, 2, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 2, 2, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 3, 2, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 2, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 0, 3, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 1, 3, 'FixedWall', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 2, 3, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 3, 3, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 3, 'FixedWall', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 4, 'FixedWall', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 5, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 5, 5, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 6, 5, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 7, 5, 'FixedWall', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 8, 5, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 6, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 5, 6, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 6, 6, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 7, 6, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 8, 6, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 7, 'FixedWall', 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 5, 7, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 6, 7, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 7, 7, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 8, 7, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 4, 8, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 5, 8, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 6, 8, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 7, 8, 'FixedWall', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 8, 8, 'FixedWall', 'N');

INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 0, 4, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 1, 4, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 2, 4, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 3, 4, 'AnimatedWallUp', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 5, 4, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 6, 4, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 7, 4, 'AnimatedWallDown', 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Camper', 8, 4, 'AnimatedWallDown', 'N');

-- Hey FAAN !!!
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,    5 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,     6 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,     8 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,   11 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,     2 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,    3 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,     4 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,     8 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   9  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  9   ,   11 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,     2 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,    3 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,     4 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,    5 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,     8 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  10  ,    10 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 10   ,   11 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,     2 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,    3 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,     4 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,    5 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,     6 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,     8 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,    9 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  11  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan', 11   ,   11 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,     0 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,    1 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,     2 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,    3 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,     4 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,    7 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,     8 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   0  ,    10 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  0   ,   11 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,     0 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,     2 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,    3 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,     4 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,    7 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,     8 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,    9 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   1  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  1   ,   11 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,     0 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,    1 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,     2 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,    3 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,     4 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,    7 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,     8 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   2  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  2   ,   11 , 'FixedWall'           , 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,    1 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,     2 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,    3 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,     4 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,     8 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,    9 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   3  ,    10 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  3   ,   11 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,    1 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,     2 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,    3 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,     4 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,     8 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   4  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  4   ,   11 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,     2 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,    3 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,     4 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,    5 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,     8 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,    9 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   5  ,    10 , 'AnimatedWallUp'     ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  5   ,   11 , 'AnimatedWallUp'      , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,     2 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,    3 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,     4 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,    5 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,     8 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,    9 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   6  ,    10 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  6   ,   11 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,     2 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,    3 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,     4 , 'AnimatedWallDown'   ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,    5 , 'AnimatedWallDown'    , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,     6 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,    7 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,     8 , 'FixedWall'          ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,    9 , 'AnimatedWallUp'      , 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   7  ,    10 , 'AnimatedWallUp'     ,  'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  7   ,   11 , 'AnimatedWallUp'      , 'Y');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,     0 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,    1 , 'Floor'               , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,     2 , 'Floor'              ,  'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',  8   ,    3 , 'FixedWall'           , 'N');
INSERT INTO Tiles (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE) VALUES ('Hey Faan',   8  ,     4 , 'FixedWall'          ,  'N');


-- Insertions dans Players_position
-- Goalie
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Goalie', 1, 4, 0);
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Goalie', 2, 3, 7);

-- Containers
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Containers', 1, 1, 1);
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Containers', 2, 7, 7);

-- Camper
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Camper', 1, 1, 7);
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Camper', 2, 5, 3);

-- Hey FAAN !!!
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Hey Faan', 1, 1, 1);
INSERT INTO Players_position (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y) VALUES ('Hey Faan', 2, 1, 10);


COMMIT;