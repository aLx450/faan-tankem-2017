 ----------------------------
| Éditeur de Niveaux | Instructions
 ----------------------------
 1. Cliquez sur les boutons "+" et "-" pour changer la taille du terrain et / ou les timers si désiré
 2. Utilisez la souris pour sélectionner le type de case voulu dans la barre de droite. Vous pouvez cocher la case "Avec arbre" ou non. Notez qu'un maximum de 4 arbres est permis
 3. Une fois le type de case choisi, cliquez et déplacez votre curseur de souris sur le terrain pour changer les cases de votre choix
 4. Pour établir les positions de départ, cliquez le bouton "Choisir" du Joueur de votre choix et cliquez ensuite sur la case voulue dans le terrain
 5. Si un Tank existe déjà pour ce joueur sur le terrain, il sera repositionné à l'endroit cliqué.
 6. N'oubliez pas de remplir la case "Nom du niveau"
 7. Pour sélectionenr le statut de votre niveau, cliquez sur un des 3 boutons radios
 8. Vous pouvez réinitialiser le niveau en tout temps en appuyant sur le bouton "Réinitialiser"
 9. Cliquez sur le bouton "Sauvegarder le niveau" pour terminer la création
 10. Si des erreurs surviennent, elles seront indiquées