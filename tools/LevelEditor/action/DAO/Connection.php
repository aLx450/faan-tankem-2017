<?php
	class Connection {
		private static $connection;

		public static function getConnection() {
			if (Connection::$connection == null) {
				try
				{
					Connection::$connection = new PDO(DB_NAME, DB_USER, DB_PASS);
					Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				}
				catch (PDOException $e)
				{
					return $e->getMessage();
				}
			}
			
			return Connection::$connection;
		}

		public static function closeConnection() {
			Connection::$connection = null;
		}

	}