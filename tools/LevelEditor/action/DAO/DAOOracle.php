<?php
	require_once("action/DAO/Connection.php");
	require_once("action/constants.php");

	class DAOOracle {
		
		public static function update($dtoLevel)
		{
			$error = "";

			$connection = Connection::getConnection();

			// Si erreur de connexion
			if (is_string($connection))
			{
				$error = DAOOracle::addError($error, "Erreur de connexion à la base de données");
			}
			else
			{
				// Insert dans Levels
				$insert = $connection->prepare("INSERT INTO LEVELS (NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY)
																	VALUES (?, ?, ?, ?, ?, ?)");
				$insert->bindParam(1, $dtoLevel->name);
				$insert->bindParam(2, $dtoLevel->status);
				$insert->bindParam(3, $dtoLevel->nbColonnes);
				$insert->bindParam(4, $dtoLevel->nbLignes);
				$insert->bindParam(5, $dtoLevel->minSpawnTime);
				$insert->bindParam(6, $dtoLevel->maxSpawnTime);

				try
				{
					$insert->execute();
				}
				catch (PDOException $e)
				{
					$error = DAOOracle::addError($error, "Erreur d'insertion dans la table Levels. Vérifiez que le nom est unique.");
				}

				// Faire le reste des insertions seulement s'il n'y a pas encore d'erreur
				if (strlen($error) == 0)
				{
					// Insert dans Tiles
					foreach ($dtoLevel->tilesArray as $tile)
					{
						// Faire l'insertion seulement si ce n'est pas une tuile vide
						if (!($tile->type == "empty" && !$tile->hasTree))
						{
							$insert = $connection->prepare("INSERT INTO TILES (LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE)
																	VALUES (?, ?, ?, ?, ?)");
							$insert->bindParam(1, $dtoLevel->name);
							$insert->bindParam(2, $tile->posCol);
							$insert->bindParam(3, $tile->posLigne);
							$type = "Floor";
							if ($tile->type == "wall"){
								$type = "FixedWall";
							}
							else if ($tile->type == "moving-wall"){
								$type = "AnimatedWallUp";
							}
							else if ($tile->type == "moving-wall-reverse"){
								$type = "AnimatedWallDown";
							}
							$insert->bindParam(4, $type);
							$hasTree = 'N';
							if ($tile->hasTree){
								$hasTree = 'Y';
							}
							$insert->bindParam(5, $hasTree);

							try
							{
								$insert->execute();
							}
							catch (PDOException $e)
							{
								$error = DAOOracle::addError($error, "Erreur d'insertion dans la table Tiles");
							}
						}
					}

					// Insert dans Players_position
					$i = 1;
					foreach ($dtoLevel->hashtablePositions as $position)
					{
						$insert = $connection->prepare("INSERT INTO PLAYERS_POSITION (LEVEL_NAME, PLAYER_NO, POS_X, POS_Y)
																	VALUES (?, ?, ?, ?)");
						$insert->bindParam(1, $dtoLevel->name);
						$insert->bindParam(2, $i);
						$insert->bindParam(3, $position[0]);
						$insert->bindParam(4, $position[1]);

						try
						{
							$insert->execute();
						}
						catch (PDOException $e)
						{
							$error = DAOOracle::addError($error, "Erreur d'insertion dans la table Players_position");
						}

						++$i;
					}	
				}
			}

			return $error;
		}

		private static function addError($error, $specificError)
		{
			// Ajouter l'erreur si non existante déjà
			if (strpos($error, $specificError) === FALSE){
				// Saut de ligne si nécéssaire
				if (strlen($error) > 0){
					$error = "\n" . $error;
				}
				$error = $error . $specificError;
			}

			return $error;
		}
	}



