<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/DAOOracle.php");
	require_once("action/DAO/DTOLevel.php");

	class AjaxAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}


		protected function executeAction() {
			if (!empty($_POST["but"])) {

				//Si l'appel vise à dessiner une tuile
				if ($_POST["but"] === "tileDraw") {
					$data["tileType"] = $_POST["tileTypeToDraw"];
					$this->result = $data;
				}

				// Si l'appel vise a vérifier la validité des choix de lignes et de colonnes
				else if ($_POST["but"] === "verifLineSize"){
					$data = [];
					if ($_POST["linesInput"] < MINLINES){
						$nbLinesSanitized = MINLINES;
						$data["message"] = "Nombre de lignes trop petit\r";
					}
					else if ($_POST["linesInput"] > MAXLINES){
						$nbLinesSanitized = MAXLINES;
						$data["message"] = "Nombre de lignes trop grand\r";
					}
					else{
						$nbLinesSanitized = intval($_POST["linesInput"]);
					}
					$data["nbResult"] = $nbLinesSanitized;

					$this->result = $data;
				}
				else if ($_POST["but"] === "verifColumnSize"){
					$data = [];
					if ($_POST["columnsInput"] < MINCOLUMNS){
						$nbColumnsSanitized = MINCOLUMNS;
						$data["message"] = "Nombre de colonnes trop petit\r";
					}
					else if ($_POST["columnsInput"] > MAXCOLUMNS){
						$nbColumnsSanitized = MAXCOLUMNS;
						$data["message"] = "Nombre de colonnes trop grand\r";
					}
					else{
						$nbColumnsSanitized = intval($_POST["columnsInput"]);
					}
					
					$data["nbResult"] = $nbColumnsSanitized;
					$this->result = $data;

				}
				else if ($_POST["but"] === "verifMinTime"){
					$data = [];
					if ($_POST["minTimeInput"] < MINMINTIME){
						$nbMintime = MINMINTIME;
						$data["message"] = "Temps indiqué inférieur au minimum accepté pour le temps minimal\r";
					}
					else if ($_POST["minTimeInput"] > MAXMINTIME){
						$nbMintime = MAXMINTIME;
						$data["message"] = "Temps indiqué supérieur au maximum accepté pour le temps minimal\r";
					}
					else if ($_POST["minTimeInput"] > $_POST["maxTimeInput"] ){
						$nbMintime =  $_POST["maxTimeInput"];
						$data["message"] = "Le temps minimal ne peut dépasser le temps maximal\r";
					}
					else{
						$nbMintime = intval($_POST["minTimeInput"]);
					}
					
					$data["nbResult"] = $nbMintime;
					$this->result = $data;

				}
				else if ($_POST["but"] === "verifMaxTime"){
					$data = [];
					if ($_POST["maxTimeInput"] < MINMAXTIME){
						$nbMaxtime = MINMAXTIME;
						$data["message"] = "Temps indiqué inférieur au minimum accepté pour le temps maximal\r";
					}
					else if ($_POST["maxTimeInput"] > MAXMAXTIME){
						$nbMaxtime = MAXMAXTIME;
						$data["message"] = "Temps indiqué supérieur au maximum accepté pour le temps maximal\r";
					}
					else if ($_POST["maxTimeInput"] < $_POST["minTimeInput"] ){
						$nbMaxtime =  $_POST["minTimeInput"];
						$data["message"] = "Le temps maximal ne peut être inférieur au temps minimal\r";
					}
					else{
						$nbMaxtime = intval($_POST["maxTimeInput"]);
					}
					
					$data["nbResult"] = $nbMaxtime;
					$this->result = $data;

				}
				else if ($_POST["but"] === "soumettreNiveau"){
					// Objet envoyé au DTOLevel
					$data = [];

					// Variables passées dans l'objet 
					$statusLevel = null;
					$name = null;
					$nbLignes = null; // done
					$nbColonnes = null; // done

					$minSpawnTime = null; // done
					$maxSpawnTime = null; // done
					$tilesArray = array();
					$positionsJoueurs = array();
	
					$nbLignes = $_POST["linesInput"];
					$nbColonnes = $_POST["columnsInput"];
					$maxSpawnTime = $_POST["maxTimeInput"];
					$minSpawnTime =  $_POST["minTimeInput"];
					$name =  $_POST["levelName"];
					$statusLevel = $_POST["levelStatus"];
					$tilesArray = $_POST["tilesArray"];
					$positionsJoueurs = $_POST["positionsJoueurs"];

					$data["nbLignes"] = json_decode($nbLignes);
					$data["nbColonnes"] = json_decode($nbColonnes);
					$data["maxSpawnTime"] = json_decode($maxSpawnTime);
					$data["minSpawnTime"] = json_decode($minSpawnTime);
					$data["name"] = $name;
					$data["statusLevel"] = json_decode($statusLevel);
					$data["tilesArray"] = json_decode($tilesArray);
					$data["positionsJoueurs"] = json_decode($positionsJoueurs);
										
					$levelDTO = new DTOLevel($data);
					
					$this->result = DAOOracle::update($levelDTO);




				}
				
			}
			else {
				// Envoi vide.
				
			}

		}
	}