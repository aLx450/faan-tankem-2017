// Variable du canevas, pour usage global
var playerID = null
var tankOneImage = null
var tankTwoImage = null
var tree = null
var ctx = null;
var canvas = null;
var spriteList = [];
var imageEmpty = null;
var currentTileType = "empty";

var h =  {};
h['wall'] = 'images/wall.jpg';
h['empty'] = 'images/grass.jpg';
h['moving-wall'] = 'images/up-wall.jpg';
h['moving-wall-reverse'] = 'images/down-wall.jpg';

var isDragging = false;
var down = null;
var isTreeActivated = false;
var currentTreeCount = 0;

// Équivalent du window.onload mais version JQuery
// Gestion des événements: à faire seulement après chargement de la page en entier. 
$(function () {
	// Init des assets visuels pour eviter les erreurs de chargement d'image
	imageEmpty = new Image();
	imageEmpty.src= 'images/grass.jpg';

	tree = new Image()
	tree.src = "images/tree.png"

	tankOneImage = new Image()
	tankOneImage.src = "images/red-tank.png"

	tankTwoImage = new Image()
	tankTwoImage.src = "images/blue-tank.png"
	
	var imageDefault= new Image();	
	imageDefault.src= 'images/grass.jpg';

	canvas = $('#canvas');

	// Click event binding pour la fenêtre d'aide
	$("#help").click(function(){
		$(".instructions-data").dialog()
	})

	// Click binding pour les boutons de positions des joueurs
	$("#apply-pos-player1").click(function(){
		playerID = 1
		deselectTileType()
		
	})

	$("#apply-pos-player2").click(function(){
		playerID = 2
		deselectTileType()
	})

	// Vérification du click and drag.
	canvas.bind({
	
		mousedown : function(e){
		down = true;  
		},
		mousemove : function(e){
			if(down && (currentTileType != null)){
				drawTile(e.offsetX, e.offsetY);

				if (isTreeActivated == true && currentTreeCount <4){
					drawTree(e.offsetX, e.offsetY);
					currentTreeCount++
				}

			}
		},
		mouseup : function(e){
			down = false;

			// Adding tanks
			if(playerID != null){
				drawTank(e.offsetX, e.offsetY)
				playerID = null
			}
		}
	})

	// Toggle arbre
	$("#checkbox").click(function(e){
		if ($("#checkbox").is(':checked')){
			isTreeActivated = true;
		}
		else{
			isTreeActivated = false;
		}
	});

	// Ajout-suppression des lignes
	$("#remove-line").click(function (e) {
		// Changer la valeur (négativement, de 1, premier paramètre) affichée par la node (passée en deuxième paramètre). 
		affect(false, document.getElementById("input-lines"));
		// Déclencher manuellement un événement de type "change" donnant le même résultat qu'une modification du textbox par clavier.
		$("#input-lines").change();
		
	});
	$("#add-line").click(function (e) {
		// Changer la valeur (positivement, de 1, premier paramètre) affichée par la node (passée en deuxième paramètre).
		affect(true, document.getElementById("input-lines"));	
		$("#input-lines").change();	
	});

	// Ajout-suppression des colonnes
	$("#remove-column").click(function (e) {
		affect(false, document.getElementById("input-columns"));
		$("#input-columns").change();	
	});

	$("#add-column").click(function (e) {
		affect(true, document.getElementById("input-columns"));	
		$("#input-columns").change();
	});

	// Temps minimum d'apparition des power-ups
	$("#remove-min-time").click(function (e) {
		affect(false, document.getElementById("input-min-time"));	
		$("#input-min-time").change();
	});

	$("#add-min-time").click(function (e) {
		affect(true, document.getElementById("input-min-time"));	
		$("#input-min-time").change();
	});

	// Temps maximum d'apparition des power-ups
	$("#remove-max-time").click(function (e) {
		affect(false, document.getElementById("input-max-time"));	
		$("#input-max-time").change();
		
	});

	$("#add-max-time").click(function (e) {
		affect(true, document.getElementById("input-max-time"));	
		$("#input-max-time").change();
	});


	// Fonction qui prend compte du changement dans les temps, lignes et colonnes si l'usager les entre par "clavier" plutot que les boutons
	$("#input-min-time").change(function (e) { 
		nodeValeurMin = document.getElementById("input-min-time").value;
		nodeValeurMax = document.getElementById("input-max-time").value;
		$.ajax({
			type : "POST",
			url : "ajax.php",
			data : {
				but: "verifMinTime",
				minTimeInput : nodeValeurMin,
				maxTimeInput : nodeValeurMax
			}
		})
		.done(function (response) {
			var result = JSON.parse(response);
			nodeLineInput = document.getElementById("input-min-time");
			nodeLineInput.value=result["nbResult"];
			manageMessage(result["message"], document.getElementById("errorLC"));
		})

		// fin du  .change function
	});

	$("#input-max-time").change(function (e) {
		nodeValeurMin = document.getElementById("input-min-time").value;
		nodeValeurMax = document.getElementById("input-max-time").value;

		$.ajax({
			type : "POST",
			url : "ajax.php",
			data : {
				but: "verifMaxTime",
				minTimeInput : nodeValeurMin,
				maxTimeInput : nodeValeurMax
			}
		})
		.done(function (response) {
			var result = JSON.parse(response);
			nodeLineInput = document.getElementById("input-max-time");
			nodeLineInput.value=result["nbResult"];
			manageMessage(result["message"], document.getElementById("errorLC"));
		})

		// fin du  .change function
	});


	$("#input-lines").change(function (e) {
		nodeLigneValeur = document.getElementById("input-lines").value;

		$.ajax({
			type : "POST",
			url : "ajax.php",
			data : {
				but: "verifLineSize",
				linesInput : nodeLigneValeur
			}
		})
		.done(function (response) {
			var result = JSON.parse(response);
			nodeLineInput = document.getElementById("input-lines");
			nodeLineInput.value=result["nbResult"];
			manageMessage(result["message"], document.getElementById("errorLC"));
			document.getElementById("input-lines").value = result["nbResult"];
			drawGrid();
		})

		// fin du  .change function
	});

	$("#input-columns").change(function (e) {
		nodeColonneValeur = document.getElementById("input-columns").value
		
		$.ajax({
			type : "POST",
			url : "ajax.php",
			data : {
				but: "verifColumnSize",
				columnsInput : nodeColonneValeur
			}
		})
		.done(function (response) {
			var result = JSON.parse(response);
			nodeColonneInput = document.getElementById("input-columns");
			nodeColonneInput.value = result["nbResult"];
		
			manageMessage(result["message"], document.getElementById("errorLC"));
			document.getElementById("input-columns").value = result["nbResult"];
			drawGrid();
		})
		// fin du  .change function
	});

	
	$("#empty").click(function (e) {
		document.getElementById("wall").style.border="";
		document.getElementById("moving-wall").style.border="";
		document.getElementById("moving-wall-reverse").style.border="";

		if (currentTileType == "empty" ){
			document.getElementById("empty").style.border="";
			currentTileType =null;
		}
		else{
			document.getElementById("empty").style.border="thick solid #ffd700";
			currentTileType = "empty";
		}

		// fin du  .click function
	});

	$("#wall").click(function (e) {
		document.getElementById("empty").style.border="";
		document.getElementById("moving-wall").style.border="";
		document.getElementById("moving-wall-reverse").style.border="";

		if (currentTileType == "wall" ){
			document.getElementById("wall").style.border="";
			currentTileType =null;
		}
		else{
			document.getElementById("wall").style.border="thick solid #ffd700";
			currentTileType = "wall";
		}

		// fin du  .change function
	});


	$("#moving-wall").click(function (e) {	

		document.getElementById("wall").style.border="";
		document.getElementById("empty").style.border="";
		document.getElementById("moving-wall-reverse").style.border="";

		if (currentTileType == "moving-wall" ){
			document.getElementById("moving-wall").style.border="";
			currentTileType =null;
		}
		else{
			document.getElementById("moving-wall").style.border="thick solid #ffd700";
			currentTileType = "moving-wall";
		}

		// fin du  .change function
	});


	$("#moving-wall-reverse").click(function (e) {
		document.getElementById("wall").style.border="";
		document.getElementById("moving-wall").style.border="";
		document.getElementById("empty").style.border="";

		if (currentTileType =="moving-wall-reverse"){
			document.getElementById("moving-wall-reverse").style.border="";
			currentTileType =null;
		}
		else{
			document.getElementById("moving-wall-reverse").style.border="thick solid #ffd700";	
			currentTileType = "moving-wall-reverse";
		}

		// fin du  .change function
	});

	// Tracer la grille initiale
	imageDefault.onload = function(e){
		drawGrid();

		spriteList.forEach(
			function (element){
				element.image = imageDefault;
				element.affectImage(imageDefault);
				element.affectType(currentTileType);
				
				ctx.lineWidth = 2;
				ctx.strokeRect(element.x, element.y,element.size, element.size); 
			}
		);
	}
	// fin du onload
})


function getLevelStatus(){
		var checkedID = null
		if($('#active').is(':checked')){
			checkedID = "Actif"
		}
		else if($('#inactive').is(':checked')){
			checkedID = "Inactif"
		}
		else if($('#test').is(':checked')){
			checkedID = "Test"
		}

		return checkedID
}

function deselectTileType(){
	document.getElementById("wall").style.border="";
	document.getElementById("moving-wall").style.border="";
	document.getElementById("moving-wall-reverse").style.border="";
	document.getElementById("empty").style.border="";
	currentTileType = null
}

// Mise a jour des champs de position des Tanks
function setPositionBoxes(element, id){
	if(id == 1){
		$("#line-pos-player1").val(element.posLigne + 1)
		$("#column-pos-player1").val(element.posCol + 1)
	}
	else if(id == 2){
		$("#line-pos-player2").val(element.posLigne +1)
		$("#column-pos-player2").val(element.posCol +1)
	}
}

// Vider les champs de positions des tanks
function clearPositionBoxes(element, id){
	if(id == 1){
		$("#line-pos-player1").val("")
		$("#column-pos-player1").val("")
	}
	else if(id == 2){
		$("#line-pos-player2").val("")
		$("#column-pos-player2").val("")
	}

}

// Dessiner arbre
function drawTree(posX, posY){
	spriteList.forEach(
		function (element){
			if (posX > element.x && posX < (element.x+element.size) && posY >element.y && posY <  (element.y+element.size))
			{
				element.affectTree(true);
			}
		}

	);
}

// Dessiner tank
function drawTank(posX, posY){

	var hasTankOneFound = false;
	var hasTankTwoFound = false;
	spriteList.forEach(
		function (element){
			if (posX > element.x && posX < (element.x+element.size) && posY >element.y && posY <  (element.y+element.size))
			{
				// Voir si une case a deja un tank et si oui, remplacer l'image
				spriteList.forEach(function(elementchecked){
					if (playerID == 1 ){
						if (elementchecked.hasTankOne){
							element.hasTank = false;
							elementchecked.hasTankOne = false;
							elementchecked.affectImage(elementchecked.image);
						}
					}
					else if (playerID == 2){
						if (elementchecked.hasTankTwo){
							element.hasTank = false;
							elementchecked.hasTankTwo = false;
							elementchecked.affectImage(elementchecked.image);
						}
					}
				})

				// Placer le tank a l'endroit cliqué
				element.affectTank(playerID, true);
				setPositionBoxes(element, playerID)
			}
		}
	);
}

// Fonction de dessin
function drawTile(posX, posY){

	spriteList.forEach(
		function (element){
			if (posX > element.x && posX < (element.x+element.size) && posY >element.y && posY <  (element.y+element.size))
			{
				// Verif s'il y a arbre
				if (element.hasTree){
					currentTreeCount--;
					element.hasTree = false;
				}

				// Remove tanks
				if(element.hasTank){
					element.hasTank = false
				}

				if(element.hasTankOne){
					element.hasTankOne = false;
					clearPositionBoxes(element, 1);
				}

				if(element.hasTankTwo){
					element.hasTankTwo = false
					clearPositionBoxes(element, 2);
				}

				// Dessiner et affecter le plus récent contenu de la Tile
				var imageToDraw= new Image();
				imageToDraw.src= h[currentTileType];
				element.image = imageToDraw;
				element.affectImage(imageToDraw);
				element.affectType(currentTileType);
				
				ctx.lineWidth=2;
				ctx.strokeRect(element.x, element.y,element.size, element.size); 
			}
		}
	);
}

// Fonction d'incrémentation ou de décrémentation des valeurs des cases colonnes, lignes et temps
function affect(plusmoins, elementId) {
    if (plusmoins){
		elementId.value = (Number(elementId.value)+1);
	}
	else{
		elementId.value = (Number(elementId.value)-1);
	}
}

// Gestion des messages d'erreurs à même l'éditeur de texte. 
function manageMessage(aVerifier, elementId){
	if (aVerifier){
		elementId.innerHTML = aVerifier;
		elementId.style.display = "block";
			setTimeout(function(){		
				elementId.style.display = "none";
		}, 1500);
	}
	else{
		elementId.innerHTML = "";
		elementId.style.display = "none";
	}
}

// Fonction pour dessiner, sur le canevas, un "tableau" de lignes et de colonnes selon l'input de l'usager
function drawGrid(){
	currentTreeCount = 0;

	// Spritelist temporaire qui contient un "snapshot" de l'état précédent de la map
	var previousSpriteList = [];
	
	// Remplir cette liste temporaire avec le contenu du "snapshot"
	for(var i = 0; i < spriteList.length; i++){
		previousSpriteList.push(spriteList[i])
	}
	spriteList.splice(0,spriteList.length)

	
	ctx = document.getElementById("canvas").getContext("2d");
	ctx.strokeStyle = "black";
	var nbLignes = document.getElementById("input-columns").value;
	var nbColonnes = document.getElementById("input-lines").value;
	var canvasWidth = document.getElementById("canvas").offsetWidth;
	var canvasHeight = document.getElementById("canvas").offsetHeight;
	var padding = 10;

	ctx.clearRect(0,0,600,600);
	
	var relativeTileSize = (canvasWidth-(2*padding))/(Math.max(nbLignes, nbColonnes));
	imageEmpty.width = relativeTileSize;
	imageEmpty.height = relativeTileSize;

	var nbCasesMadeY = 0;
	var tankOneFound = false;
	var tankTwoFound = false;

	// Boucle pour parcourir le nombre de tiles adéquat selon la taille actuelle du terrain
	for (var x = 0; x < nbLignes; x+=1)
	{
		for (var y = 0; y < nbColonnes; y+=1){
				var tileFound = false;

				// Parcourir la spriteList temporaire et voir une tuile existait a cet endroit dans le "snapshot"
				for (var i = 0; i < previousSpriteList.length; i++){
					if (previousSpriteList[i].posLigne === y && previousSpriteList[i].posCol === x){
						tileFound = true;

						// S'il y a arbre
						if (previousSpriteList[i].hasTree){
							currentTreeCount++;
						}

						// S'il y a tank
						if(previousSpriteList[i].hasTankOne)
						{
							tankOneFound = true;
						}
						else if (previousSpriteList[i].hasTankTwo){
							tankTwoFound = true;
						}

						// Affecter les images et les dimensions aux tuiles
						previousSpriteList[i].x = (x*relativeTileSize)+padding;
						previousSpriteList[i].y = (y*relativeTileSize)+padding;
						previousSpriteList[i].size = relativeTileSize;
						previousSpriteList[i].affectImage(previousSpriteList[i].image);

						// Mettre les tuiles trouvées dans la spritelist courante
						spriteList.push(previousSpriteList[i]);
					}
				}

				// Si on ne trouve pas de tuile et cet endroit, on en insère une nouvelle
				if (tileFound == false){
					 spriteList.push(new Tile((x*relativeTileSize)+padding,(y*relativeTileSize)+padding,relativeTileSize, imageEmpty, y, x));
				}

				// Tracer les tuiles
				ctx.lineWidth=2;
				ctx.strokeRect((x*relativeTileSize)+padding, (y*relativeTileSize)+padding,relativeTileSize,relativeTileSize); 
				nbCasesMadeY+=1;
		}
	}

	// Mettre a jour les champs de positions des tanks
	if (!tankOneFound){
		document.getElementById("line-pos-player1").value = "";
		document.getElementById("column-pos-player1").value = "";
	}
	if (!tankTwoFound){
		document.getElementById("line-pos-player2").value = "";
		document.getElementById("column-pos-player2").value = "";
	}

	// Vider la spritelist temporaire
	previousSpriteList.splice(0,previousSpriteList.length)
}