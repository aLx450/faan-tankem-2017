var nbPlayers = 2; 

$(function () {
	$("#save").click(function (e) {

		var nbLignes = document.getElementById("input-lines").value;
		var nbColonnes = document.getElementById("input-columns").value;
		var maxTime =  document.getElementById("input-max-time").value;
		var minTime =  document.getElementById("input-min-time").value;
		var nomNiveau = document.getElementById("level-name").value;
		var statutNiveau = getLevelStatus();
		var positionsJoueurs = []; // array de arrays. chaque sous array a en 0 la pos ligne, en 1 la pos col

		spriteList.forEach( 
			function(element){
				var posArray = [];
				if (element.hasTankOne){
					posArray.push(element.posCol);
					posArray.push(element.posLigne);
					positionsJoueurs.push(posArray);
				}
				else if (element.hasTankTwo){
					posArray.push(element.posCol);
					posArray.push(element.posLigne);
					positionsJoueurs.push(posArray);
				}
			
			}
		)

		// S'il manque des informations obligatoires, les 4 ifs suivant gerent cette eventualite et empechent l'appel ajax.
		var errorMessage = "";
		if (nomNiveau == "" || nomNiveau == null){
			errorMessage += "Un nom de niveau est obligatoire. \n";
		}
		// if (positionsJoueurs[0][0] == null || positionsJoueurs[0][1] == null || positionsJoueurs[1][0] == null || positionsJoueurs[1][1] == null){
		if (positionsJoueurs.length < 2){
			errorMessage += "Une position de départ pour chaque joueur est obligatoire. \n";
		}
		if (statutNiveau == null || statutNiveau == ""){
			errorMessage += "Vous devez spécifier le statut du niveau actuel. \n";
		}


		if (errorMessage != ""){
			popMessage("Erreur", errorMessage);
		}
		else{
			console.log(spriteList);
			$.ajax({
				type : "POST",
				url : "ajax.php",
				data : {
					but: "soumettreNiveau",
					columnsInput : nbColonnes,
					linesInput : nbLignes, 
					maxTimeInput : maxTime,
					minTimeInput : minTime,
					levelName :  nomNiveau,
					levelStatus : JSON.stringify(statutNiveau),
					tilesArray: JSON.stringify(spriteList),
					positionsJoueurs: JSON.stringify(positionsJoueurs)
				}
			})
			.done(function (response) {
				var resultat = JSON.parse(response);
				if (resultat != "") {
					popMessage("Erreur", resultat);
				} else {
					var messagePopUp = "<p style='font-weight: bold;'>Le niveau a été sauvegardé !</p><p> Voulez-vous réinitialiser le terrain ?</p>";
					popMessage("Succès", messagePopUp, function () {
						clearFields();
						drawGrid();
					});
				}
			})
			.fail(function (response){
				popMessage("Erreur", "Une erreur s'est produite lors du stockage de votre niveau.");
			})
			

		}

	});

	$("#reset").click(function(){
		clearFields()
		drawGrid()
	});

	function clearFields(){
		document.getElementById("")
		document.getElementById("level-name").value = "";

		var elements = document.getElementsByName("choice");
		for(var i = 0 ; i < elements.length; ++i){
			elements[i].checked = false;
		}
		spriteList.splice(0,spriteList.length)
	}

	function popMessage(title, message, func = "close")
	{
		document.getElementById("dialog-message").innerHTML = message;
		document.getElementById("dialog-message").setAttribute("title", title);
		if (func != "close")
		{
			$( "#dialog-message" ).dialog({
				modal: true,
				buttons: {
					Oui: function() {
						func();
						$( this ).dialog( "close" );						
					},
					Non: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		else
		{
			$( "#dialog-message" ).dialog({
				modal: true,
				buttons: {
					Ok: function() {
						$( this ).dialog( func );
					}
				}
			});
		}
	}
})