class Tile {

	constructor(x, y, size, imageParam, posLigne, posCol){
		this.x = x;
		this.y = y;
		this.size = size;
		this.image = imageParam;
		this.affectImage(this.image);
		this.posLigne = posLigne;
		this.posCol = posCol;
		this.type = 'empty';
		this.hasTree = false;
		this.hasTank = false;
		this.hasTankOne = false;
		this.hasTankTwo = false;
	
	}

	affectType(tileType){
		this.type = tileType;
		
	}

	affectTree(value){
		this.hasTree = value;
		this.affectImage(this.image);
		
	}

	affectTank(id, value) {
		this.hasTank = value;
		this.tankID = id;
			if(this.tankID == 1) {
				this.hasTankOne = true;
			}
			else if(this.tankID == 2){
				this.hasTankTwo = true
			}
		
		this.affectImage(this.image);
	}

	affectImage(img){
		ctx.drawImage(img, this.x, this.y, this.size, this.size);

		if (this.hasTree){
			ctx.drawImage(tree, this.x + this.size / 2, this.y + this.size / 2, this.size / 2, this.size / 2)
		}

		if(this.hasTank){
			if(this.hasTankOne){
				ctx.drawImage(tankOneImage, this.x + this.size / 6, this.y + this.size / 6, this.size - (2 * (this.size / 6)), this.size - (2 * (this.size / 6)))
			}
			else if(this.hasTankTwo){
				ctx.drawImage(tankTwoImage, this.x + this.size / 6, this.y + this.size / 6, this.size - (2 * (this.size / 6)), this.size - (2 * (this.size / 6)))
			}
			
		}
		ctx.strokeRect(this.x, this.y, this.size, this.size)
	}	
}