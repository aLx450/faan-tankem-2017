<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<div class="container">
		<div class="header">
			<h1>Level Editor | Projet Tankem</h1>
		</div>
		<!-- end header -->

		<div class="inner-container">
			
		<div class="top-frame">
			<div class="left-column">
				<div class="editor-data">
					<canvas width="600px" height="600px" id="canvas"></canvas>
				</div>
				<!-- end editor data -->

				<div class="terrain-data">
					<div class="size-editor">
						<h2>Taille du terrain</h2>
						<div class="lines">
							<h3>Lignes</h3>
							<button type="text" id="remove-line">-</button>
							<input type="text" id="input-lines" value="6">
							<button type="text" id="add-line">+</button>
						</div>
						<div class="columns">
							<h3>Colonnes</h3>
							<button type="text" id="remove-column">-</button>
							<input type="text" id="input-columns" value="6">
							<button type="text" id="add-column">+</button>
						</div>
						<div class="clear"></div>
					</div>
					<div class="timer-editor">
						<h2>Temps des spawns</h2>
						<div class="min-timer">
							<h3>Minimum</h3>
							<button type="text" id="remove-min-time">-</button>
							<input type="text" id="input-min-time" value="2">
							<button type="text" id="add-min-time">+</button>
						</div>
						<div class="max-timer">
							<h3>Maximum</h3>
							<button type="text" id="remove-max-time">-</button>
							<input type="text" id="input-max-time" value="6">
							<button type="text" id="add-max-time">+</button>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
					<div class="error" id="errorLC"></div>
				</div>
				<!--end terrain data-->
			</div>
			<!--end left-column-->

			<div class="right-column">
			<button id="help" type="text">Aide</button>
				<div class="tile-data">
					<div class="type" id="empty"><p>Herbe</p></div>
					<div class="type" id="wall"><p>Plateforme</p></div>
					<div class="type" id="moving-wall"><p>Plateforme ascendante</p></div>
					<div class="type" id="moving-wall-reverse"><p>Plateforme descendate</p></div>
				</div>
				
				<div class="tree-data">
					<div class="tree-info">
						<label id="tree-label" for="checkbox">Avec arbre?</label>
						<input id="checkbox" type="checkbox">
					</div>
				</div>
				<!-- end tile data -->

				<div class="positions-data">
					<div class="positions-info">
						<h2>Positions de départ</h2>
						<div class="player1">
							<label for="pos-player1">Joueur 1 : </label>
							<label for="line-pos-player2">L</label>
							<input type="text" id="line-pos-player1" placeholder="Ligne" readonly>
							<label for="column-pos-player2">C</label>
							<input type="text" id="column-pos-player1" placeholder="Col" readonly>
							<button type="text" id="apply-pos-player1">Choisir</button>
						</div>
						<div class="player2">
							<label for="pos-player2">Joueur 2 : </label>
							<label for="line-pos-player2">L</label>
							<input type="text" id="line-pos-player2" placeholder="Ligne" readonly>
							<label for="column-pos-player2">C</label>
							<input type="text" id="column-pos-player2" placeholder="Col" readonly>
							<button type="text" id="apply-pos-player2">Choisir</button>
						</div>
					</div>
				</div>
				<!-- end player data -->
			</div>
			<!--end right column-->
		</div> 
		<!--end top frame-->

		<div class="clear"></div>

		<div class="bottom-frame">
			<div class="level-data">
				<div class="level-info">
					<label for="level-name">Nom du niveau : </label>
					<input id="level-name" maxlength="20" type="text">
					<div class="level-status">
						<h3>Statut du niveau</h3>
						<label for="active">Active</label>
						<input type="radio" id="active" name="choice">
						<label for="inactive">Inactive</label>
						<input type="radio" id="inactive" name="choice">
						<label for="test">Test</label>
						<input type="radio" id="test" name="choice">
					</div>
					<div>
						<button id="save" type="text">Sauvegarder le niveau</button>
						<button id="reset" type="text">Réinitialiser</button>
					</div>
				</div>
				<!-- end level info -->
			</div>
			<!-- end level data -->	

			<div class="instructions-data" title="Instructions">
				<div class="instructions-info">
					<ul>
						<li>Cliquez sur les boutons <span>"+"</span> et <span>"-"</span> pour changer la taille du terrain et / ou les timers si désiré</li>
						<li>Utilisez la souris pour sélectionner le type de case voulu dans la barre de droite. Vous pouvez cocher la case <span>"Avec arbre"</span> ou non. Notez qu'un <span>maximum de 4 arbres</span> est permis</li>
						<li>Une fois le type de case choisi, <span>cliquez et déplacez</span> votre curseur de souris sur le terrain pour changer les cases de votre choix</li>
						<li>Pour établir les positions de départ, cliquez le bouton <span>"Choisir"</span> du Joueur de votre choix et cliquez ensuite sur la case voulue dans le terrain</li>
						<li>Si un Tank existe déjà pour ce joueur sur le terrain, il sera repositionné à l'endroit cliqué.</li>
						<li>N'oubliez pas de remplir la case <span>"Nom du niveau"</span></li>
						<li>Pour sélectionenr le statut de votre niveau, cliquez sur un des <span>3 boutons radios</span></li>
						<li>Vous pouvez réinitialiser le niveau en tout temps en appuyant sur le bouton <span>"Réinitialiser"</span></li>
						<li>Cliquez sur le bouton <span>"Sauvegarder le niveau"</span> pour terminer la création</li>
						<li>Si des erreurs surviennent, elles seront <span id="error-span">indiquées</span></li>
					</ul>
				</div>
			</div>
			<!-- end instructions data -->
			<div id="dialog-message"></div>
		</div>
		<!--end bottom frame-->
		</div>
		<div class="clear"></div>
	</div>
	<!-- end container -->

<?php
	require_once("partial/footer.php");
	?>