-- IMPORTANT : Assurez-vous d'avoir exécuté le script dropAllTables.cmd avant n'importe quel autre de cette liste

-- Creation des tables
CREATE TABLE GAMESAVE(
	saveID NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	gameID NUMBER NOT NULL
);

CREATE TABLE ITEMSAVE(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	saveID NUMBER NOT NULL,
	armeName varchar2(25) NOT NULL,
	armeX NUMBER NOT NULL,
	armeY NUMBER NOT NULL,
	armeZ NUMBER NOT NULL, 
	CONSTRAINT fk_saveInItem FOREIGN KEY (saveID) REFERENCES GAMESAVE(saveID)
); 

CREATE TABLE BULLETSAVE(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	saveID NUMBER NOT NULL,
	balleActive NUMBER(1) NOT NULL CHECK  (balleActive in (0,1)),
	balleX NUMBER NOT NULL,
	balleY NUMBER NOT NULL,
	balleZ NUMBER NOT NULL,
	weaponName varchar2(25), 
	ingameID NUMBER NOT NULL,
	shooterID NUMBER NOT NULL,
	CONSTRAINT fk_saveInBullets FOREIGN KEY (saveID) REFERENCES GAMESAVE(saveID)
); 

-- ENLEVER Le TANK1 et TANK2. CLÉS primaires composites !=_id mais plutot COMBINAISON de SAVEID et TANKID
CREATE TABLE TANKSAVE(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	saveID NUMBER NOT NULL,
	JoueurID NUMBER NOT NULL,
	tankX NUMBER NOT NULL,
	tankY NUMBER NOT NULL,
	tankZ NUMBER NOT NULL,
	tankH NUMBER NOT NULL,
	tankP NUMBER NOT NULL,
	tankR NUMBER NOT NULL,
	tankHP NUMBER NOT NULL,
	tankHasShot NUMBER(1) NOT NULL CHECK  (tankHasShot in (0,1)),
	tankWeapon varchar2(25), 
	CONSTRAINT fk_saveInTank FOREIGN KEY (saveID) REFERENCES GAMESAVE(saveID),
	CONSTRAINT fk_joueurInTank FOREIGN KEY (JoueurID) REFERENCES Users(ID)
);

-- ENLEVER Le JOUEUR1 et JOUEUR2. CLÉS primaires composites !=_id mais plutot COMBINAISON de GAMEID et JOUEURID
-- REFERENCE A LA TABLE SQL DES JOUEURS.
-- AJouter... #CONSTRAINT fk_gameInPlayer FOREIGN KEY (GameID) REFERENCES GAMESAVE(ID),
CREATE TABLE PLAYERSAVE(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	GameID NUMBER NOT NULL,
	JoueurID NUMBER NOT NULL,
	TankCouleur varchar2(25) NOT NULL,
	Level_Name varchar2(25) NOT NULL,
	Vie NUMBER NOT NULL,
	Force NUMBER NOT NULL,
	Agilite NUMBER NOT NULL,
	Dexterite NUMBER NOT NULL,
	UserName varchar2(50) NOT NULL,
	NomCompose varchar2(50),
	CONSTRAINT fk_joueurInPlayer FOREIGN KEY (JoueurID) REFERENCES Users(ID),
	CONSTRAINT fk_LevelInPlayer FOREIGN KEY (Level_Name) REFERENCES Levels(NAME)
);

CREATE TABLE GAMERES(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	GameID NUMBER NOT NULL,
	FrameSkip NUMBER NOT NULL
);

-- Contraintes
ALTER TABLE ITEMSAVE
ADD CONSTRAINT check_weapon_nameI1
  CHECK (armeName IN ('Shotgun','Grenade', 'Piege', 'Homing', 'Spring', 'Mitraillette', 'Guide'));

ALTER TABLE TANKSAVE
ADD CONSTRAINT check_weapon_name
  CHECK (tankWeapon IN ('Shotgun','Grenade', 'Piege', 'Homing', 'Spring', 'Mitraillette', 'Guide', 'Canon' ));

ALTER TABLE BULLETSAVE
ADD CONSTRAINT check_weapon_nameBulletSavew
  CHECK (weaponName IN ('Shotgun','Grenade', 'Piege', 'Homing', 'Spring', 'Mitraillette', 'Guide', 'Canon' ));

COMMIT;