# -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
import sys

from common.internal.DAO.daoLevelOracle import *

class InterfaceMenuPrincipal(ShowBase):
    def __init__(self):
        # Charger les levels
        # Création du DAOLevelOracle pour communiquer avec la base de données
        daoLevel = DAOLevelOracle()
        # Récupération des Niveaux dans la base de données
        dtosLevel = daoLevel.read()
        # Si une erreur de base de données est arrivée
        self.errorDialog = None

        # Charger la font
        self.fontTankem = loader.loadFont('../asset/Font/KBDunkTank.ttf')
        self.fontBase = loader.loadFont('../asset/Font/KeepCalm.ttf')

        #Image d'arrière plan
        self.background=OnscreenImage(
                                        parent = render2d,
                                        image = "../asset/Menu/menuPrincipal.jpg"
                                     )

        #Titre du jeu
        self.textTitre = OnscreenText(
                                        text = "Tankem!",
                                        pos = (0, 0.7), 
                                        scale = 0.32,
                                        fg = (0, 0, 0, 1),
                                        align = TextNode.ACenter,
                                        font = self.fontTankem
                                     )

        # Paramètres bouton général
        self.btnScale = 0.10
        self.borderW = (0.035, 0.035)
        self.couleurBack = (0.243, 0.325, 0.121, 1)

        # # Construction du bouton Aléatoire
        bAleatoire = DirectButton(
                                    text = ("Aléatoire", "Go", "Aléatoire", "Disabled"),
                                    text_scale = self.btnScale,
                                    borderWidth = self.borderW,
                                    text_bg = self.couleurBack,
                                    frameColor = self.couleurBack,
                                    relief = 2,
                                    command = self.connexionUser,
                                    text_font = self.fontBase
                                 )        

        # Création du label pour séparer les différents types de niveaux
        labelActive = DirectLabel(
                                        text = "Active",
                                        text_scale = self.btnScale,
                                        text_bg = (0, 0, 0, 0),
                                        text_font = self.fontBase,
                                        frameColor = (0, 0, 0, 0)
                                      )

        # Paramètres de la liste
        numItemsVisible = 4
        itemHeight = 0.20

        # Création de la liste avec les deux boutons pour parcourir la liste
        self.levelList = DirectScrolledList(
                                                decButton_pos = (-0.2, 0, -0.42),
                                                decButton_text = ("Monte", "Haut", "Monte", "Disabled"),
                                                decButton_text_scale = 0.07,
                                                decButton_relief = 2,
                                                decButton_borderWidth = self.borderW,
                                                decButton_text_font = self.fontBase,
                                                decButton_frameColor = (1, 1, 1, 0.8),

                                                incButton_pos = (0.2, 0, -0.42),
                                                incButton_text = ("Baisse", "Bas", "Baisse", "Disabled"),
                                                incButton_text_scale = 0.07,
                                                incButton_relief = 2,
                                                incButton_borderWidth = self.borderW,
                                                incButton_text_font = self.fontBase,
                                                incButton_frameColor = (1, 1, 1, 0.8),

                                                frameSize = (-0.5, 0.5, -0.5, 0.6),
                                                frameColor = (0, 0, 0, 0.4),
                                                pos = (0, 0, 0),
                                                items = [labelActive, bAleatoire],

                                                numItemsVisible = numItemsVisible,
                                                forceHeight = itemHeight,

                                                itemFrame_frameSize = (-0.4, 0.4, -0.7, 0.15),
                                                itemFrame_frameColor = (1, 1, 1, 0.6),
                                                itemFrame_pos = (0, 0, 0.4)
                                            )

        # Création du bouton pour quitter le jeu
        self.b2 = DirectButton(
                                text = ("Quitter", "Bye bye!", ":-(", "Disabled"),
                                text_scale = self.btnScale,
                                borderWidth = self.borderW,
                                text_bg = self.couleurBack,
                                frameColor = self.couleurBack,
                                relief = 2,
                                command = lambda : sys.exit(),
                                pos = (0, 0, -0.7),
                                text_font = self.fontBase
                              )

        # Permet de remplir la liste des niveaux
        self.fillLevelList(dtosLevel)

    # Permet la création du dialog quand une erreur est trouvée
    def createDialog(self):
        self.errorDialog = OkDialog(
                                        dialogName = "Erreur",
                                        text = "Un problème de connexion à la base de données",
                                        command = self.clearErrorDialog
                                   )

    # Supprimer le dialog
    def clearErrorDialog(self, args):
        self.errorDialog.cleanup()

    # Remplissage de la liste des niveaux
    def fillLevelList(self, listDTO):
        dtos, erreur = listDTO
        # Si une erreur est renvoyée
        if len(erreur) > 0:
            self.createDialog()
        else: # Sinon
            if dtos is not None:
                dtoTest = []
                for level in dtos:
                    if level.status == DTOLevel.STATUS_TEST:
                        dtoTest.append(level)
                    if level.status == DTOLevel.STATUS_ACTIVE:
                        bLevel = self.creerBouton(level, self.couleurBack)
                        self.levelList.addItem(bLevel)

                labelTesting = DirectLabel(
                                            text = "Testing",
                                            text_scale = self.btnScale,
                                            text_bg = (0, 0, 0, 0),
                                            text_font = self.fontBase,
                                            frameColor = (0, 0, 0, 0)
                                        )
                self.levelList.addItem(labelTesting)
                for levelTest in dtoTest:
                    colorBack = (0.725, 0.725, 0, 1)
                    bLevel = self.creerBouton(levelTest, colorBack)
                    self.levelList.addItem(bLevel)

    # Permet de créer un nouveau bouton
    def creerBouton(self, level, colorBack):
        bLevel = DirectButton(
                                text = (level.name, "Go", level.name, "Disabled"),
                                text_scale = self.btnScale,
                                borderWidth = self.borderW,
                                text_bg = colorBack,
                                frameColor = colorBack,
                                relief = 2,
                                command = lambda lvl = level: self.connexionUser(lvl),
                                text_font = self.fontBase
                             )
        return bLevel

    def cacher(self):
        #Est esssentiellement un code de "loading"
        #On cache les menus
        self.background.hide()
        self.levelList.hide()
        self.b2.hide()
        self.textTitre.hide()

    def connexionUser(self, dtoLevel = None):
        if dtoLevel is not None:
            messenger.send("ConnexionUser", [dtoLevel,])
        else:
            messenger.send("ConnexionUser")
            
        self.cacher()