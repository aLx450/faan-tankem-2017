# -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
from direct.interval.LerpInterval import LerpPosInterval

from common.internal.colors import *
from common.internal.DAO.daoJoueurOracle import *

import threading
import webbrowser

class InterfaceConnexion(ShowBase):
	def __init__(self, dtoLevel):
		self.daoJoueur		= DAOJoueurOracle()
		self.listeDTOJoueur	= []
		self.dtoLevel		= dtoLevel

		# Charger la font
		self.fontBase	= loader.loadFont('../asset/Font/KeepCalm.ttf')
		self.infosLogin	= []

		# Paramètres bouton général
		self.btnScale				= 0.12
		self.borderW				= (0.035, 0.035)
		self.couleurBack			= (0.243, 0.325, 0.121, 1)
		self.couleurBtnAttributs	= (0, 0.475, 0.475, 1)

		# Modele de tank
		self.tankOne = None
		self.tankTwo = None
		
		#On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
		self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
		base.cam.node().getDisplayRegion(0).setSort(20)

		self.playerOneBigName = None
		self.playerTwoBigName = None
		self.vsBig = None

		# ------ PHASE 1 ------ #

		# Frame Principal Phase 1
		self.framePrincipalPhaseOne = DirectFrame()

		# Frame Joueur 1
		self.framePlayer1 = DirectFrame(	parent		= self.framePrincipalPhaseOne,
											frameColor	= (0, 0, 0, 0.6),
                      						frameSize	= (-0.1, 1.5, -0.25, 0.1),
											pos			= (-1.55, 0, 1.28)
									   )

		# Texte Nom Player 1										  
		self.textNamePlayer1 = self.creerTextNode("textNamePlayer1", "Joueur 1", (-2.8, 0, 0.85), 0.085, self.fontBase, (1, 1, 1, 1))

		# Champ Nom Player 1
		self.entryNamePlayer1 = DirectEntry(	parent			= self.framePrincipalPhaseOne,
												scale			= 0.05,
												pos				= Vec3(-2.3, 0, 0.86), 
												numLines		= 1,
												focus			= 1,
												command			= self.getValue,
												focusOutCommand	= lambda: self.changerFocus(self.entryPasswordPlayer1),
												focusInCommand	= self.viderCache
										   )

		# Texte Mot de passe Player 1
		self.textPasswordPlayer1 = self.creerTextNode("textPasswordPlayer1", "Mot de passe", (-2.8, 0, 0.70), 0.085, self.fontBase, (1, 1, 1, 1))

		# Champ Mot de passe Player 1
		self.entryPasswordPlayer1 = DirectEntry(	parent		= self.framePrincipalPhaseOne,
													scale		= 0.05,
													pos			= Vec3(-2.3, 0, 0.71),
													numLines	= 1,
													obscured	= True,
													command		= self.checkLogin
											   )

		# Frame Joueur 2
		self.framePlayer2 = DirectFrame(	parent		= self.framePrincipalPhaseOne,
											frameColor	= (0, 0, 0, 0.6),
											frameSize	= (-0.1, 1.5, -0.25, 0.1),
											pos			= (0.15, 0, 1.28)
									   )

		# Texte Nom Player 2
		self.textNamePlayer2 = self.creerTextNode("textNamePlayer2", "Joueur 2", (1.9, 0, 0.85), 0.085, self.fontBase, (1, 1, 1, 1))

		# Champ Nom Player 2
		self.entryNamePlayer2 = DirectEntry(	parent			= self.framePrincipalPhaseOne,
												scale			= 0.05,
												pos				= Vec3(2.3, 0, 0.86),
												numLines		= 1,
												command			= self.getValue,
												focusOutCommand	= lambda: self.changerFocus(self.entryPasswordPlayer2),
												focusInCommand	= self.viderCache,
												state			= 0
										   )

		# Texte Mot de passe Player 2
		self.textPasswordPlayer2 = self.creerTextNode("textPasswordPlayer2", "Mot de passe", (2.3, 0, 0.70), 0.085, self.fontBase, (1, 1, 1, 1))

		# Champ Mot de passe Player 2
		self.entryPasswordPlayer2 = DirectEntry(	parent		= self.framePrincipalPhaseOne,
													scale		= 0.05,
													pos			= Vec3(2.3, 0, 0.71),
													numLines	= 1,
													obscured	= True,
													command		= self.checkLogin,
													state		= 0
											   )

		# Frame Message
		self.frameMessage = DirectFrame(	parent		= self.framePrincipalPhaseOne,
											frameColor	= (0, 0, 0, 0.6),
											frameSize	= (-1.3, 1.3, -0.1, 0.1),
											pos			= (0, 0, 0.5)
									   )

		# Texte Message
		self.textMessage = OnscreenText(	parent	= self.framePrincipalPhaseOne,
											pos		= (0, 0.47),
											scale	= 0.07,
											fg		= (1, 1, 1, 1),
											align	= TextNode.ACenter,
											font	= self.fontBase
									   )

		# Affiche le frameMessage au début du programme
		self.setInitialErrorText()

		# ------ PHASE 2 ------ #

		# Frame Principal Phase 2
		self.framePrincipalPhaseTwo = DirectFrame()

		# self.textPasswordPlayer2 = self.creerTextNode("textPasswordPlayer2", "Mot de passe", (2.3, 0, 0.70), 0.085, self.fontBase, (1, 1, 1, 1))
		#Frame Adversaires
		self.frameAdversaires = DirectFrame(	parent		= self.framePrincipalPhaseTwo,
												frameColor	= (0, 0, 0, 0.6),
												frameSize	= (-1.3, 1.3, -0.25, 0.25),
												pos			= (-2.5, 0, 0.7)
										   )

		# Texte nom calculé Joueur 1
		self.nomCalcPlayer1 = None

		# Texte VS
		self.textVS = self.creerTextNode("textVS", "VS", (2.5, 0, 0.66), 0.11, self.fontBase, (1, 1, 1, 1))

		# Texte Nom calculé Joueur 2
		self.nomCalcPlayer2 = None

		# Frame Level
		self.frameLevel = DirectFrame(	parent		= self.framePrincipalPhaseTwo,
										frameColor	= (0, 0, 0, 0.6),
										frameSize	= (-1.3, 1.3, -0.15, 0.15),
										pos			= (2.5, 0, 0.22)
									 )

		# Texte Combattre
		self.combattre = self.creerTextNode("combattre", "Champ de bataille :", (-2.5, 0, 0.26), 0.05, self.fontBase, (1, 1, 1, 1))
		
		# Texte Nom Level
		self.level = None

		# Frame Favori
		self.frameFavori = DirectFrame(	parent		= self.framePrincipalPhaseTwo,
										frameColor	= (0, 0, 0, 0.6),
										frameSize	= (-1.3, 1.3, -0.12, 0.12),
										pos			= (-2.5, 0, -0.13)
									  )

		# Texte Favori
		self.favori = self.creerTextNode("favori", "Favori :", (2.5, 0, -0.1), 0.06, self.fontBase, (1, 1, 1, 1))

		# Texte Nom Favori
		self.nomFavori = None
		
		# Bouton FIGHT
		self.btnFight = DirectButton(	parent		= self.framePrincipalPhaseTwo,
										text		= ("Fight!", "Go!", "Fight!", "Disabled"),
										text_scale	= self.btnScale,
										borderWidth	= self.borderW,
										text_bg		= self.couleurBack,
										frameColor	= self.couleurBack,
										relief		= 2,
										command		= self.gamePrep,
										text_font	= self.fontBase,
										pos			= Vec3(0, 0, -2.5)
									)

		# Bouton WEB
		self.btnWeb = DirectButton(	text		= ("Services en ligne", "Go!", "Attributs!", "Disabled"),
									text_scale	= self.btnScale - 0.03,
									scale		= 0.85,
									borderWidth	= self.borderW,
									text_bg		= self.couleurBtnAttributs,
									frameColor	= self.couleurBtnAttributs,
									relief		= 2,
									command		= self.openWebsite,
									text_font	= self.fontBase,
									pos			= Vec3(1.2, 0, -0.90)
								  )

		#Image d'arrière plan
		self.background = OnscreenImage(	parent	= render2d,
											image	= "../asset/Menu/menuPrincipal.jpg"
									   )

		# Cache la phase 2 au début du programme
		self.framePrincipalPhaseTwo.hide()

		# #Initialisation de l'effet de transition
		curtain = loader.loadTexture("../asset/Menu/loading.jpg")

		self.transition = Transitions(loader)
		self.transition.setFadeColor(0, 0, 0)
		self.transition.setFadeModel(curtain)

		self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

		self.showPhaseOne()

	def rotateTank(self, id):
		if id == 1:
			self.tankOneRotateSequence = Sequence(self.tankOneRotate)
			self.tankOneRotateSequence.loop()
		elif id == 2:
			self.tankTwoRotateSequence = Sequence(self.tankTwoRotate)
			self.tankTwoRotateSequence.loop()

	def spawnTank(self, id, color):
		splitColor = hex2rgb(color)
		pandaColors = convertToPandaColors(splitColor[0], splitColor[1], splitColor[2])

		if id == 1:
			self.tankOne = loader.loadModel("../asset/Tank/tank")
			self.tankOne.setScale(3, 3, 3)
			self.tankOne.setColorScale(pandaColors[0], pandaColors[1], pandaColors[2], 1)
			self.tankOne.setPos(Vec3(-40, 0, -20))
			self.tankOne.setHpr(Vec3(-90, 0, -65))
			self.tankOne.reparentTo(render)
		elif id == 2:
			self.tankTwo = loader.loadModel("../asset/Tank/tank")
			self.tankTwo.setScale(3, 3, 3)
			self.tankTwo.setColorScale(pandaColors[0], pandaColors[1], pandaColors[2], 1)
			self.tankTwo.setPos(Vec3(40, 0, -20))
			self.tankTwo.setHpr(Vec3(90, 0, 65))
			self.tankTwo.reparentTo(render)

	def changerFocus(self, needsFocus):
		needsFocus['focus'] = 1

	def setMessage(self, message):
		self.textMessage.setText(message)
		threading.Timer(2.2, self.setInitialErrorText).start()

	def initialiserInfoPhaseTwo(self):
		usernameJ1	= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_USERNAME]
		usernameJ2	= self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_USERNAME]
		nomCalcJ1	= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_CALC_NAME]
		nomCalcJ2	= self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_CALC_NAME]

		self.nomCalcPlayer1 = self.creerTextNode("textNomCalcPlayer1", usernameJ1 + ", " + nomCalcJ1, (-2.5, 0, 0.8), 0.085, self.fontBase, (1, 1, 1, 1))
		self.nomCalcPlayer2 = self.creerTextNode("textNomCalcPlayer2", usernameJ2 + ", " + nomCalcJ2, (2.5, 0, 0.55), 0.085, self.fontBase, (1, 1, 1, 1))		

		if self.dtoLevel is not None:
			self.level = self.creerTextNode("level", self.dtoLevel.name, (2.5, 0, 0.15), 0.08, self.fontBase, (1, 1, 1, 1))
		else:
			self.level = self.creerTextNode("level", "Aléatoire", (2.5, 0, 0.15), 0.08, self.fontBase, (1, 1, 1, 1))

		self.nomFavori = self.creerTextNode("nomFavori", self.calculerFavori(), (-2.5, 0, -0.2), 0.08, self.fontBase, (1, 1, 1, 1))

	def calculerFavori(self):
		levelJ1		= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
		levelJ2		= self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
		usernameJ1	= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_USERNAME]
		usernameJ2	= self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_USERNAME]
		nomCalcJ1	= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_CALC_NAME]
		nomCalcJ2	= self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_CALC_NAME]

		nomFavori = "Aucun joueur favori"

		if levelJ1 > levelJ2:
			nomFavori = usernameJ1 + ", " + nomCalcJ1
		elif levelJ1 < levelJ2:
			nomFavori = usernameJ2 + ", " + nomCalcJ2

		return nomFavori

	def clearField(self, field):
		field.enterText("")

	def getValue(self, value):
		self.infosLogin.append(value)

	def viderCache(self):
		self.infosLogin = []

	def checkLogin(self, pwdContent):
		sameUser = False
		self.infosLogin.append(pwdContent)

		if len(self.listeDTOJoueur) == 1:
			usernameJ1	= self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_USERNAME]
			if self.infosLogin[0] == usernameJ1:
				sameUser = True

		if sameUser is not True:
			success, dto, errorConnexion, userFound = self.daoJoueur.validLogin(self.infosLogin)

			if errorConnexion is False:
				if userFound:
					if success:
						if len(self.listeDTOJoueur) == 0:
							self.entryNamePlayer1["state"] = 0
							self.entryPasswordPlayer1["state"] = 0
							self.entryNamePlayer2["state"] = 1
							self.entryPasswordPlayer2["state"] = 1
							self.changerFocus(self.entryNamePlayer2)
						self.listeDTOJoueur.append(dto)

						if len(self.listeDTOJoueur) == 1:
							color = self.listeDTOJoueur[-1].hashtable[DTOJoueur.KEY_TANK_COLOR]
							self.spawnTank(1, color)
							tankOneSlide = LerpPosInterval(self.tankOne, 1, Vec3(-10, 0, -20), blendType='easeOut')
							self.tankOneRotate = self.tankOne.hprInterval(3.0, Vec3(-90, 360, -65), blendType = "easeOut")

							self.tankOneRotateSequence = None
							self.sequenceTankOne = Sequence(tankOneSlide, Wait(1), Func(self.rotateTank, 1)).start()

						elif len(self.listeDTOJoueur) == 2:
							self.entryNamePlayer2["state"] = 0
							self.entryPasswordPlayer2["state"] = 0
							color = self.listeDTOJoueur[-1].hashtable[DTOJoueur.KEY_TANK_COLOR]
							self.spawnTank(2, color)
							tankTwoSlide = LerpPosInterval(self.tankTwo, 1, Vec3(10, 0, -20), blendType='easeOut')
							self.tankTwoRotate = self.tankTwo.hprInterval(3.0, Vec3(90, 360, 65), blendType = "easeOut")

							self.tankTwoRotateSequence = None
							self.sequenceTankTwo = Sequence(tankTwoSlide, Wait(1), Func(self.rotateTank, 2)).start()
							threading.Timer(2.5, self.slideOutPhaseOne).start()
							threading.Timer(3.0, self.cacherPhaseOne).start()
						
						self.sequenceBtnFight = None

						usernameJoueur	= self.listeDTOJoueur[-1].hashtable[DTOJoueur.KEY_USERNAME]
						nomCalcJoueur	= self.listeDTOJoueur[-1].hashtable[DTOJoueur.KEY_CALC_NAME]

						self.setMessage("Bienvenue " + usernameJoueur + ", " + nomCalcJoueur)
						self.infosLogin = []

					else:
						del self.infosLogin[-1]
						if len(self.listeDTOJoueur) == 0:
							self.clearField(self.entryPasswordPlayer1)
							self.changerFocus(self.entryPasswordPlayer1)
						else:
							self.clearField(self.entryPasswordPlayer2)
							self.changerFocus(self.entryPasswordPlayer2)
						self.setMessage("Le mot de passe n'est pas valide")
				else:
					if len(self.listeDTOJoueur) == 0:
						self.clearField(self.entryPasswordPlayer1)
						self.changerFocus(self.entryNamePlayer1)
					else:
						self.clearField(self.entryPasswordPlayer2)
						self.changerFocus(self.entryNamePlayer2)
					self.infosLogin = []
					self.setMessage("Utilisateur introuvable")
			else:
				self.setMessage("Erreur de connexion à la BD")
				self.chargeJeu()
		else:
			self.clearField(self.entryPasswordPlayer2)
			self.changerFocus(self.entryNamePlayer2)
			self.infosLogin = []
			self.setMessage("L’utilisateur est déjà connecté")
	
	def openWebsite(self):
		webbrowser.open('http://localhost/b63-web-tankem/Web/')

	def slideOutPhaseOne(self):
		framePlayerOneSlide = LerpPosInterval(self.framePlayer1, 0.75, Vec3(-1.55, 0, 1.28), blendType='easeOut').start()
		textNamePlayerOneSlide = LerpPosInterval(self.textNamePlayer1, 0.75, Vec3(-2.8, 0, 0.85), blendType='easeOut').start()
		entryNamePlayerOneSlide = LerpPosInterval(self.entryNamePlayer1, 0.75, Vec3(-2.3, 0, 0.86), blendType='easeOut').start()
		textPasswordPlayerOneSlide = LerpPosInterval(self.textPasswordPlayer1, 1, Vec3(-2.8, 0, 0.70), blendType='easeOut').start()
		entryPasswordPlayerOneSlide = LerpPosInterval(self.entryPasswordPlayer1, 1, Vec3(-2.3, 0, 0.71), blendType='easeOut').start()

		framePlayerTwoSlide = LerpPosInterval(self.framePlayer2, 0.75, Vec3(0.15, 0, 1.28), blendType='easeOut').start()
		textNamePlayerOneSlide = LerpPosInterval(self.textNamePlayer2, 0.75, Vec3(1.9, 0, 0.85), blendType='easeOut').start()
		entryNamePlayerTwoSlide = LerpPosInterval(self.entryNamePlayer2, 0.75, Vec3(2.3, 0, 0.86), blendType='easeOut').start()
		textPasswordPlayerOneSlide = LerpPosInterval(self.textPasswordPlayer2, 1, Vec3(2.3, 0, 0.70), blendType='easeOut').start()
		entryPasswordPlayerTwoSlide = LerpPosInterval(self.entryPasswordPlayer2, 1, Vec3(2.3, 0, 0.71), blendType='easeOut').start()

	def cacherPhaseOne(self):
		self.framePrincipalPhaseOne.hide()
		self.framePlayer1.hide()
		self.textNamePlayer1.hide()
		self.textPasswordPlayer1.hide()

		self.framePlayer2.hide()
		self.textNamePlayer2.hide()
		self.textPasswordPlayer2.hide()

		self.initialiserInfoPhaseTwo()
		threading.Timer(0.75, self.showPhaseTwo).start()

	def setInitialErrorText(self):
		self.textMessage.setText("Appuyez sur Entrée pour changer de champ.")

	def slideOutPhaseTwo(self):
		frameAdversairesSlide = LerpPosInterval(self.frameAdversaires, 0.75, Vec3(-2.5,0,0.7), blendType='easeOut').start()
		textVSSlide = LerpPosInterval(self.textVS, 0.75, Vec3(2.5, 0, 0.66), blendType='easeOut').start()
		textNomCalcPlayer1Slide = LerpPosInterval(self.nomCalcPlayer1, 0.75, Vec3(-2.5, 0, 0.8), blendType='easeOut').start()
		textNomCalcPlayer2Slide = LerpPosInterval(self.nomCalcPlayer2, 0.75, Vec3(2.5, 0, 0.55), blendType='easeOut').start()

		frameLevelSlide = LerpPosInterval(self.frameLevel, 0.75, Vec3(2.5,0,0.22), blendType='easeOut').start()
		textCombattreSlide = LerpPosInterval(self.combattre, 0.75, Vec3(-2.5,0,0.26), blendType='easeOut').start()
		textLevelSlide = LerpPosInterval(self.level, 0.75, Vec3(2.5,0,0.15), blendType='easeOut').start()
		
		frameFavoriSlide = LerpPosInterval(self.frameFavori, 0.75, Vec3(-2.5,0,-0.13), blendType='easeOut').start()
		textFavoriSlide = LerpPosInterval(self.favori, 0.75, Vec3(2.5,0,-0.1), blendType='easeOut').start()
		textNomFavoriSlide = LerpPosInterval(self.nomFavori, 0.75, Vec3(-2.5,0,-0.2), blendType='easeOut').start()

		btnFightSlide = LerpPosInterval(self.btnFight, 2, Vec3(0, 0, -2.5), blendType='easeOut').start()

		btnWebSlide = LerpPosInterval(self.btnWeb, 2, Vec3(0, 0, -2.5), blendType='easeOut').start()

	def cacherPhaseTwo(self):
		self.framePrincipalPhaseTwo.hide()
		self.nomFavori.hide()
		self.textVS.hide()
		self.combattre.hide()
		self.favori.hide()
		self.nomCalcPlayer1.hide()
		self.nomCalcPlayer2.hide()
		self.level.hide()
		self.btnWeb.hide()
		self.btnFight.hide()

	def cacherBtnWeb(self):
		self.btnWeb.hide()

	def showPhaseOne(self):
		framePlayerOneSlide = LerpPosInterval(self.framePlayer1, 0.75, Vec3(-1.55, 0, 0.88), blendType='easeOut').start()
		textNamePlayerOneSlide = LerpPosInterval(self.textNamePlayer1, 0.75, Vec3(-1.20, 0, 0.85), blendType='easeOut').start()
		entryNamePlayerOneSlide = LerpPosInterval(self.entryNamePlayer1, 0.75, Vec3(-0.70, 0, 0.86), blendType='easeOut').start()
		textPasswordPlayerOneSlide = LerpPosInterval(self.textPasswordPlayer1, 1, Vec3(-1.20, 0, 0.70), blendType='easeOut').start()
		entryPasswordPlayerOneSlide = LerpPosInterval(self.entryPasswordPlayer1, 1, Vec3(-0.70, 0, 0.71), blendType='easeOut').start()

		framePlayerTwoSlide = LerpPosInterval(self.framePlayer2, 0.75, Vec3(0.15, 0, 0.88), blendType='easeOut').start()
		textNamePlayerOneSlide = LerpPosInterval(self.textNamePlayer2, 0.75, Vec3(0.50, 0, 0.85), blendType='easeOut').start()
		entryNamePlayerTwoSlide = LerpPosInterval(self.entryNamePlayer2, 0.75, Vec3(1, 0, 0.86), blendType='easeOut').start()
		textPasswordPlayerOneSlide = LerpPosInterval(self.textPasswordPlayer2, 1, Vec3(0.50, 0, 0.70), blendType='easeOut').start()
		entryPasswordPlayerTwoSlide = LerpPosInterval(self.entryPasswordPlayer2, 1, Vec3(1, 0, 0.71), blendType='easeOut').start()

	def showPhaseTwo(self):
		self.framePrincipalPhaseTwo.show()
		
		frameAdversairesSlide = LerpPosInterval(self.frameAdversaires, 0.75, Vec3(0,0,0.7), blendType='easeOut').start()
		textVSSlide = LerpPosInterval(self.textVS, 0.75, Vec3(0, 0, 0.66), blendType='easeOut').start()
		textNomCalcPlayer1Slide = LerpPosInterval(self.nomCalcPlayer1, 0.75, Vec3(0, 0, 0.8), blendType='easeOut').start()
		textNomCalcPlayer2Slide = LerpPosInterval(self.nomCalcPlayer2, 0.75, Vec3(0, 0, 0.55), blendType='easeOut').start()

		frameLevelSlide = LerpPosInterval(self.frameLevel, 0.75, Vec3(0,0,0.22), blendType='easeOut').start()
		textCombattreSlide = LerpPosInterval(self.combattre, 0.75, Vec3(0,0,0.26), blendType='easeOut').start()
		textLevelSlide = LerpPosInterval(self.level, 0.75, Vec3(0,0,0.15), blendType='easeOut').start()
		
		frameFavoriSlide = LerpPosInterval(self.frameFavori, 0.75, Vec3(0,0,-0.13), blendType='easeOut').start()
		textFavoriSlide = LerpPosInterval(self.favori, 0.75, Vec3(0,0,-0.1), blendType='easeOut').start()
		textNomFavoriSlide = LerpPosInterval(self.nomFavori, 0.75, Vec3(0,0,-0.2), blendType='easeOut').start()

		btnFightSlide = LerpPosInterval(self.btnFight, 1, Vec3(0,0,-0.85), blendType='easeOut').start()

		btnFightScaleUp = LerpScaleInterval(self.btnFight, 0.25, 0.6)
		btnFightScaleDown = LerpScaleInterval(self.btnFight, 0.25, 1)
		self.sequenceBtnFight = Sequence(btnFightScaleUp, btnFightScaleDown)
		self.sequenceBtnFight.loop()

	def creerTextNode(self, nomNode, content, pos, scale, font, fg):
		textNode = TextNode(nomNode)
		textNode.setText(content)
		textNode.setAlign(TextNode.ACenter)
		textNode.setFont(font)
		textNode.setTextColor(fg)
		textNodePath = aspect2d.attachNewNode(textNode)
		textNodePath.setPos(pos)
		textNodePath.setScale(scale)
		return textNodePath

	def cacher(self):
		#Est esssentiellement un code de "loading"
		self.tankOne.removeNode()
		self.tankTwo.removeNode()
		self.playerOneBigName.hide()
		self.playerTwoBigName.hide()
		self.vsBig.hide()
		self.background.hide()
		base.cam.node().getDisplayRegion(0).setSort(self.baseSort)

	def preGameAnim(self, nameOne, nameTwo, colorOne, colorTwo):
		splitColorOne = hex2rgb(colorOne)
		splitColorTwo = hex2rgb(colorTwo)
		pandaColorsOne = convertToPandaColors(splitColorOne[0], splitColorOne[1], splitColorOne[2])
		pandaColorsTwo= convertToPandaColors(splitColorTwo[0], splitColorTwo[1], splitColorTwo[2])
		self.sequenceBtnFight.finish()

		self.playerOneBigName = self.creerTextNode("playerOne", nameOne, (-2.5, 0, 0.5), 0.3, self.fontBase, (pandaColorsOne[0], pandaColorsOne[1], pandaColorsOne[2], 1))
		self.vsBig = self.creerTextNode("vs", "vs", (0, 0, -2.5), 0.2, self.fontBase, (1, 1, 1, 1))
		self.playerTwoBigName = self.creerTextNode("playerTwo", nameTwo, (2.5, 0, -0.5), 0.3, self.fontBase, (pandaColorsTwo[0], pandaColorsTwo[1], pandaColorsTwo[2], 1))

		slideInNameOne = LerpPosInterval(self.playerOneBigName, 1, Vec3(0,0,0.3))
		slideInVs = LerpPosInterval(self.vsBig, 1, Vec3(0,0,0))
		slideInNameTwo = LerpPosInterval(self.playerTwoBigName, 1, Vec3(0,0,-0.3))
		slideSequence = Sequence(slideInNameOne, slideInVs, slideInNameTwo)
		slideSequence.start()

		preGameSeq = Sequence(Wait(5), Func(self.chargeJeu))
		preGameSeq.start()
		
	
	def gamePrep(self):
		self.slideOutPhaseTwo()
		threading.Timer(0.5, self.cacherPhaseTwo).start()

		actualTankOneHpr = self.tankOne.getHpr()
		self.tankOneRotateSequence.finish()
		self.tankOne.setHpr(actualTankOneHpr)
		self.tankOneRotate = self.tankOne.hprInterval(3.0, Vec3(-90, 0, -65), blendType = "easeOut").start()

		actualTankTwoHpr = self.tankTwo.getHpr()
		self.tankTwoRotateSequence.finish()
		self.tankTwo.setHpr(actualTankTwoHpr)
		self.tankOneRotate = self.tankTwo.hprInterval(3.0, Vec3(90, 0, 65), blendType = "easeOut").start()

		nomJoueurUn = self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_USERNAME]
		nomJoueurDeux = self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_USERNAME]
		colorJoueurUn = self.listeDTOJoueur[0].hashtable[DTOJoueur.KEY_TANK_COLOR]
		colorJoueurDeux = self.listeDTOJoueur[1].hashtable[DTOJoueur.KEY_TANK_COLOR]

		self.preGameAnim(nomJoueurUn, nomJoueurDeux, colorJoueurUn, colorJoueurDeux)
		
	
	def chargeJeu(self):
		#On démarre!
		startGameMessage = lambda : messenger.send("DemarrerPartie", [self.dtoLevel, self.listeDTOJoueur])

		Sequence(Func(self.cacher),
				Func(lambda : self.transition.irisOut(0.2)),
				SoundInterval(self.sound),
				Func(startGameMessage),
				Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
				Func(lambda : self.transition.irisIn(0.2))
		).start()