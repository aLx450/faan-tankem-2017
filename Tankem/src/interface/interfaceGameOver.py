# -*- coding:utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
from direct.interval.LerpInterval import LerpPosInterval

import threading
import webbrowser

from common.internal.colors import *
from common.internal.DTO.dtoJoueur import *


class InterfaceGameOver(ShowBase):
	def __init__(self, gameAnalysis):
		self.gameAnalysis = gameAnalysis
		self.dtoPlayerList = gameAnalysis.dtoPlayerList
		self.tankList = gameAnalysis.tankList
		self.player1DbId = self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_ID]
		self.player2DbId = self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_ID]
		self.winnerDbId = gameAnalysis.winnerDbId
		self.winnerIndex = 0
		if (self.winnerDbId == self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_ID]):
			self.winnerIndex = 1
		self.winner = self.dtoPlayerList[self.winnerIndex]
		self.colorWhite = (1, 1, 1, 1)
		self.colorYellow = (0.921, 0.929, 0.231, 1)
		self.colorBlue = (0.321, 0.415, 0.937, 1)

		# Charger la font
		self.fontBase	= loader.loadFont('../asset/Font/KeepCalm.ttf')

		# Paramètres bouton général
		self.btnScale				= 0.12
		self.borderW				= (0.035, 0.035)

		#Image d'arrière plan
		self.background = OnscreenImage(parent= render2d, image	= "../asset/GameOver/tank.jpg")

		# Frame semi-opaque pour clarifier le texte
		self.framePlayer2 = DirectFrame(frameColor = (0, 0, 0, 0.7),frameSize= (-3, 3, -3, 3), pos	= (0, 0, 0))

		# Label de félicitation
		textCongrats = "Bien joué " + self.winner.hashtable[DTOJoueur.KEY_USERNAME] + ", " + self.winner.hashtable[DTOJoueur.KEY_CALC_NAME]
		self.lblCongrats = self.createTextNode("lblCongrats", textCongrats, (0, 0, 0.85), 0.1, self.fontBase, (1,1,1,1))

		# Label des joueurs
		player1Name = self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_USERNAME]
		player2Name = self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_USERNAME]
		player1Color = self.colorWhite
		player2Color = self.colorWhite
		if (self.winnerIndex == 0):
			player1Color = self.colorYellow
		else:
			player2Color = self.colorYellow
		self.lblPlayer1 = self.createTextNode("lblPlayer1", player1Name, (-0.8, 0, 0.4), 0.11, self.fontBase, player1Color)
		self.lblPlayer2 = self.createTextNode("lblPlayer2", player2Name, (0.8, 0, 0.4), 0.11, self.fontBase, player2Color)

		# Bonus exp
		self.exp1 = self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_EXP]
		self.exp2 = self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_EXP]
		self.bonus1 = int(self.gameAnalysis.getExpBonus(self.player1DbId))
		self.bonus2 = int(self.gameAnalysis.getExpBonus(self.player2DbId))

		# Labels exp
		self.lblExp = self.createTextNode("lblExp", "Expérience", (0, 0, 0), 0.12, self.fontBase, self.colorBlue)
		self.lblExp1 = self.createTextNode("lblExp1", str(self.exp1), (-1.2, 0, 0), 0.08, self.fontBase, (1,1,1,1))
		self.lblExp2 = self.createTextNode("lblExp2", str(self.exp2), (0.9, 0, 0), 0.08, self.fontBase, (1,1,1,1))
		self.lblBonus1 = self.createTextNode("lblBonus1", "+" + str(self.bonus1), (-0.9, 0, 0), 0.08, self.fontBase, (0.047, 0.631, 0.168,1))
		self.lblBonus2 = self.createTextNode("lblBonus2", "+" + str(self.bonus2), (1.2, 0, 0), 0.08, self.fontBase, (0.047, 0.631, 0.168, 1))

		# Niveau
		self.level1 = self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
		self.level2 = self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
		self.nextLevel1 = self.gameAnalysis.nextLevel(self.player1DbId)
		self.nextLevel2 = self.gameAnalysis.nextLevel(self.player2DbId)

		# Labels niveau
		self.lblLevel = self.createTextNode("lblLevel", "Niveau", (0, 0, -0.4), 0.12, self.fontBase, self.colorBlue)
		self.lblLevel1 = self.createTextNode("lblLevel1", str(self.level1), (-1, 0, -0.4), 0.08, self.fontBase, (1,1,1,1))
		self.lblLevel2 = self.createTextNode("lblLevel2", str(self.level2), (1, 0, -0.4), 0.08, self.fontBase, (1,1,1,1))

		# Animations
		self.seqExp1 = Sequence(Func(self.increaseExp, 1, self.lblExp1, self.lblBonus1))
		self.seqExp2 = Sequence(Func(self.increaseExp, 2, self.lblExp2, self.lblBonus2))
		threading.Timer(2, self.seqExp1.loop).start()
		threading.Timer(2, self.seqExp2.loop).start()
		self.seqLevel1 = Sequence(Func(self.increaseLevel, 1, self.lblLevel1), Wait(1))
		self.seqLevel2 = Sequence(Func(self.increaseLevel, 2, self.lblLevel2), Wait(1))


		# Boutons WEB
		self.btnWeb1 = DirectButton(text		= ("Fiche Web", "Go!", "Fiche Web", "Disabled"),
									text_scale	= self.btnScale - 0.03,
									scale		= 0.85,
									borderWidth	= self.borderW,
									text_bg		= self.tankColor(0),
									frameColor	= self.tankColor(0),
									relief		= 2,
									command		= self.openWeb1,
									text_font	= self.fontBase,
									pos			= Vec3(-1, 0, -0.85)
								  )

		self.btnWeb2 = DirectButton(text		= ("Fiche Web", "Go!", "Fiche Web", "Disabled"),
									text_scale	= self.btnScale - 0.03,
									scale		= 0.85,
									borderWidth	= self.borderW,
									text_bg		= self.tankColor(1),
									frameColor	= self.tankColor(1),
									relief		= 2,
									command		= self.openWeb2,
									text_font	= self.fontBase,
									pos			= Vec3(1, 0, -0.85)
								  )

	def increaseExp(self, seq, expNode, bonusNode):
		# Si l'incrémentation n'est pas terminée
		if int(bonusNode[1].getText()) > 0:
			expNode[1].setText(str(int(expNode[1].getText()) + 1))
			bonusNode[1].setText("+" + str(int(bonusNode[1].getText()[1:]) - 1))
		# Si l'incrémentation pour le joueur 1 est terminée
		elif seq == 1:
			# Lerp Exp
			self.seqExp1.finish()
			self.lblBonus1[0].hide()
			moveExp1 = LerpPosInterval(self.lblExp1[0], 1, Vec3(-1, 0, 0), blendType='easeIn')
			scaleExp1 = LerpScaleInterval(self.lblExp1[0], 0.1, 0.12)
			Sequence(moveExp1, scaleExp1).start()
			# Animation Level
			self.seqLevel1.loop()
		# Si l'incrémentation pour le joueur 2 est terminée
		else:
			# Lerp Exp
			self.seqExp2.finish()
			self.lblBonus2[0].hide()
			moveExp2 = LerpPosInterval(self.lblExp2[0], 1, Vec3(1, 0, 0), blendType='easeIn')
			scaleExp2 = LerpScaleInterval(self.lblExp2[0], 0.1, 0.12)
			Sequence(moveExp2, scaleExp2).start()
			# Animation Level
			self.seqLevel2.loop()

	def increaseLevel(self, seq, levelNode):
		if seq == 1:
			# Animation Level pour le joueur 1
			if int(levelNode[1].getText()) < self.nextLevel1:
				LerpScaleInterval(levelNode[0], 0.1, 1).start()
				levelNode[1].setText(str(int(levelNode[1].getText()) + 1))
				LerpScaleInterval(levelNode[0], 0.1, 0.15).start()
			else:
				self.seqLevel1.finish()
		else:
			# Animation Level pour le joueur 2
			if int(levelNode[1].getText()) < self.nextLevel2:
				LerpScaleInterval(levelNode[0], 0.1, 1).start()
				levelNode[1].setText(str(int(levelNode[1].getText()) + 1))
				LerpScaleInterval(levelNode[0], 0.1, 0.15).start()
			else:
				self.seqLevel2.finish()

	def openWeb1(self):
		webbrowser.open('http://localhost/b63-web-tankem/Web/users?player=' + self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_USERNAME])

	def openWeb2(self):
		webbrowser.open('http://localhost/b63-web-tankem/Web/users?player=' + self.dtoPlayerList[1].hashtable[DTOJoueur.KEY_USERNAME])

	def createTextNode(self, nomNode, content, pos, scale, font, fg):
		textNode = TextNode(nomNode)
		textNode.setText(content)
		textNode.setAlign(TextNode.ACenter)
		textNode.setFont(font)
		textNode.setTextColor(fg)
		textNodePath = aspect2d.attachNewNode(textNode)
		textNodePath.setPos(pos)
		textNodePath.setScale(scale)
		return (textNodePath, textNode)

	def tankColor(self, playerIndex):
		color = self.dtoPlayerList[playerIndex].hashtable[DTOJoueur.KEY_TANK_COLOR]
		splitColor = hex2rgb(color)
		pandaColors = convertToPandaColors(splitColor[0], splitColor[1], splitColor[2])
		return (pandaColors[0], pandaColors[1], pandaColors[2], 1)