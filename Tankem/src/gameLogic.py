# -*- coding: utf-8 -*-

from util import *

from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletDebugNode

#Modules de notre jeu
from map import Map
from inputManager import InputManager
from interface import *
from common import *

#Classe qui gère les phases du jeu (Menu, début, pause, fin de partie)
class GameLogic(ShowBase):
    def __init__(self,pandaBase):
        self.pandaBase = pandaBase
        self.pandaBase.enableParticles()
        self.accept("DemarrerPartie", self.startGame)
        self.accept("ConnexionUser", self.setupConnexionUser)
        self.accept("finPartie", self.finPartie)
        self.levelName = ""
        self.listCouleurTank = []
        self.listStatsJoueur = []
        
    def setup(self, dtoLevel):
        # Nom du niveau
        if dtoLevel is not None:
            self.levelName = dtoLevel.name
        else:
            self.levelName = 'Aleatoire'
        #Instancier l'objet DTO qui renseignera toutes les entités affectées.
        self.setupDTOBalanceInstance()

        #Code de Marc-André
        self.setupBulletPhysics()

        self.setupCamera()
        if dtoLevel is not None:
            width = len(dtoLevel.tiles[0])
            height = len(dtoLevel.tiles)            
            self.setupMap(width, height)
        else:
            self.setupMap()
        self.setupLightAndShadow()

        #Création d'une carte de base
        #self.carte.creerCarteParDefaut()
        if dtoLevel is not None:
            self.map.construireMapDTO(dtoLevel, self.listCouleurTank, self.listStatsJoueur)
        else:
            self.map.construireMapHasard(self.listCouleurTank, self.listStatsJoueur)

        #A besoin des éléments de la map
        self.setupControle()
        self.setupInterface()

        #Fonction d'optimisation
        #DOIVENT ÊTRE APPELÉE APRÈS LA CRÉATION DE LA CARTE
        #Ça va prendre les modèles qui ne bougent pas et en faire un seul gros
        #en faisant un seul gros noeud avec
        self.map.figeObjetImmobile()

        #DEBUG: Décommenter pour affiche la hiérarchie
        #self.pandaBase.startDirect()

        messenger.send("ChargementTermine")

    def startGame(self, dtoLevel, listDTOJoueurs):
        self.listDTOJoueurs = listDTOJoueurs

        if self.listDTOJoueurs is not None:
            statsJ1 = self.listDTOJoueurs[0].statistiques
            statsJ2 = self.listDTOJoueurs[1].statistiques
            couleurJ1 = self.listDTOJoueurs[0].hashtable[DTOJoueur.KEY_TANK_COLOR]
            couleurJ2 = self.listDTOJoueurs[1].hashtable[DTOJoueur.KEY_TANK_COLOR]
            couleurJ1RGB = hex2rgb(couleurJ1)
            couleurJ2RGB = hex2rgb(couleurJ2)
            convertedColorJ1 = convertToPandaColors(couleurJ1RGB[0], couleurJ1RGB[1], couleurJ1RGB[2])
            convertedColorJ2 = convertToPandaColors(couleurJ2RGB[0], couleurJ2RGB[1], couleurJ2RGB[2])
            self.listCouleurTank.append(Vec3(convertedColorJ1[0], convertedColorJ1[1], convertedColorJ1[2]))
            self.listCouleurTank.append(Vec3(convertedColorJ2[0], convertedColorJ2[1], convertedColorJ2[2]))
            self.listStatsJoueur.append(statsJ1)
            self.listStatsJoueur.append(statsJ2)
        else:
            self.listCouleurTank = None
            self.listStatsJoueur = None
        self.setup(dtoLevel)
        #On démarrer l'effet du compte à rebour.
        #La fonction callBackDebutPartie sera appelée à la fin
        self.interfaceMessage.effectCountDownStart(self.sanitizedDTOBalanceInstance.balance[DTOBalance.KEY_COUNTDOWNLENGTH],self.callBackDebutPartie)
        self.interfaceMessage.effectMessageGeneral(self.sanitizedDTOBalanceInstance.balance[DTOBalance.KEY_STARTTEXT],3)

    def setupConnexionUser(self, dtoLevel = None):
        self.connexionUser = InterfaceConnexion(dtoLevel)
        self.pandaBase.disableMouse()
        self.setupTransformCamera()

    def setupDTOBalanceInstance(self):
        #Création de l'instance de DTOBalance. Sanitisation de celle-ci pour des fins de propagation dans le code.
        factory = DAOBalanceFactory()
        self.DAOBalanceOracleInstance = factory.create("oracle")
        self.hashtables = self.DAOBalanceOracleInstance.read()
        
        self.SanitizerInstance = Sanitizer()
        self.sanitizedDTOBalanceInstance = self.SanitizerInstance.sanitizeClean(self.hashtables[0], self.hashtables[1],self.hashtables[2])
        

    def setupBulletPhysics(self):
        debugNode = BulletDebugNode('Debug')
        debugNode.showWireframe(True)
        debugNode.showConstraints(True)
        debugNode.showBoundingBoxes(False)
        debugNode.showNormals(False)
        self.debugNP = render.attachNewNode(debugNode)

        self.mondePhysique = BulletWorld()
        self.mondePhysique.setGravity(Vec3(0, 0, -9.81))
        self.mondePhysique.setDebugNode(self.debugNP.node())
        taskMgr.add(self.updatePhysics, "updatePhysics")

        taskMgr.add(self.updateCarte, "updateCarte")

    def setupCamera(self):
        #On doit désactiver le contrôle par défaut de la caméra autrement on ne peut pas la positionner et l'orienter
        self.pandaBase.disableMouse()

        #Le flag pour savoir si la souris est activée ou non n'est pas accessible
        #Petit fail de Panda3D
        taskMgr.add(self.updateCamera, "updateCamera")
        self.setupTransformCamera()


    def setupTransformCamera(self):
        #Défini la position et l'orientation de la caméra
        self.positionBaseCamera = Vec3(0,-18,32)
        camera.setPos(self.positionBaseCamera)
        #On dit à la caméra de regarder l'origine (point 0,0,0)
        camera.lookAt(render)

    def setupMap(self, width=10, height=10):
        #Passage de  self.sanitizedDTOBalanceInstance a l'instance de la Map pour propagation ultérieure.
        self.map = Map(self.mondePhysique, self.sanitizedDTOBalanceInstance, width, height)
        #On construire la carte comme une coquille, de l'extérieur à l'intérieur
        #Décor et ciel
        self.map.construireDecor(camera)
        #Plancher de la carte
        self.map.construirePlancher()
        #Murs et éléments de la map

    def setupLightAndShadow(self):
        #Lumière du skybox
        plight = PointLight('Lumiere ponctuelle')
        plight.setColor(VBase4(1,1,1,1))
        plnp = render.attachNewNode(plight)
        plnp.setPos(0,0,0)
        camera.setLight(plnp)

        #Simule le soleil avec un angle
        dlight = DirectionalLight('Lumiere Directionnelle')
        dlight.setColor(VBase4(0.8, 0.8, 0.6, 1))
        dlight.get_lens().set_fov(75)
        dlight.get_lens().set_near_far(0.1, 60)
        dlight.get_lens().set_film_size(30,30)
        dlnp = render.attachNewNode(dlight)
        dlnp.setPos(Vec3(-2,-2,7))
        dlnp.lookAt(render)
        render.setLight(dlnp)

        #Lumière ambiante
        alight = AmbientLight('Lumiere ambiante')
        alight.setColor(VBase4(0.25, 0.25, 0.25, 1))
        alnp  = render.attachNewNode(alight)
        render.setLight(alnp)

        #Ne pas modifier la valeur 1024 sous peine d'avoir un jeu laid ou qui lag
        dlight.setShadowCaster(True, 1024,1024)
        #On doit activer l'ombre sur les modèles
        render.setShaderAuto()

    def setupControle(self,):
        #Créer le contrôle
        #A besoin de la liste de tank pour relayer correctement le contrôle
        self.inputManager = InputManager(self.map.listeTank,self.debugNP,self.pandaBase)
        self.accept("initCam",self.setupTransformCamera)

    def setupInterface(self):
        self.interfaceTank = []
        self.interfaceTank.append(InterfaceTank(0,self.map.listeTank[0].couleur))
        self.interfaceTank.append(InterfaceTank(1,self.map.listeTank[1].couleur))

        self.interfaceMessage = InterfaceMessage(self.sanitizedDTOBalanceInstance)

    def callBackDebutPartie(self):
        #Quand le message d'introduction est terminé, on permet aux tanks de bouger
        self.inputManager.debuterControle()

    #Mise à jour du moteur de physique
    def updateCamera(self,task):
        #On ne touche pas à la caméra si on est en mode debug
        if(self.inputManager.mouseEnabled):
            return task.cont

        vecTotal = Vec3(0,0,0)
        distanceRatio = 1.0
        if (len(self.map.listeTank) != 0):
            for tank in self.map.listeTank:
                vecTotal += tank.noeudPhysique.getPos()
            vecTotal = vecTotal/len(self.map.listeTank)

        vecTotal.setZ(0)
        camera.setPos(vecTotal + self.positionBaseCamera)
        return task.cont

    #Mise à jour du moteur de physique
    def updatePhysics(self,task):
        dt = globalClock.getDt()
        messenger.send("appliquerForce")
        self.mondePhysique.doPhysics(dt)
        #print(len(self.mondePhysique.getManifolds()))

        #Analyse de toutes les collisions
        for entrelacement in self.mondePhysique.getManifolds():
            node0 = entrelacement.getNode0()
            node1 = entrelacement.getNode1()
            self.map.traiterCollision(node0, node1)
        return task.cont

    def updateCarte(self,task):
        #print task.time
        self.map.update(task.time)
        return task.cont
    
    def finPartie(self, listeTank, listeBalle, saverHelper, loserId):
        self.saverHelper = saverHelper
        self.saverHelper.mustSave = False
        self.saverHelper.finalSave(self.listDTOJoueurs)

        # Statistiques de partie
        #====================================================================================
        # Stats de tir
        shootingStats = {}
        for balle in listeBalle:
            playerId = self.listDTOJoueurs[balle.lanceurId].hashtable[DTOJoueur.KEY_ID]
            if playerId not in shootingStats.keys():
                shootingStats[playerId] = {}
            if balle.arme not in shootingStats[playerId].keys():
                shootingStats[playerId][balle.arme] = 1
            else:
                shootingStats[playerId][balle.arme] += 1

        # Chercher le ID BD des joueurs
        player1Id = self.listDTOJoueurs[0].hashtable[DTOJoueur.KEY_ID]
        player2Id = self.listDTOJoueurs[1].hashtable[DTOJoueur.KEY_ID]

        # Déterminer le gagnant
        winnerInGame = 0 if loserId == 1 else 1
        winnerBD = self.listDTOJoueurs[winnerInGame].hashtable[DTOJoueur.KEY_ID]

        # Envoyer le DTO dans la BD
        dtoGame = DTOGameStats(self.levelName, (player1Id, player2Id), winnerBD, shootingStats)
        daoGame = DAOGameStatsOracle()
        daoGame.update(dtoGame)

        # Mise à jour des stats de joueur
        #======================================================================================
        gameAnalysis = GameOverAnalysis(self.listDTOJoueurs, listeTank, dtoGame.winnerId)
        
        # extraction des données
        newData = []
        for dto in self.listDTOJoueurs:
            userId = dto.hashtable[DTOJoueur.KEY_ID]
            currentLevel = dto.hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
            currentExp = dto.hashtable[DTOJoueur.KEY_EXP]
            bonusExp = gameAnalysis.getExpBonus(userId)
            nextLevel = gameAnalysis.nextLevel(userId)
            newData.append((int(currentExp + bonusExp), int(nextLevel), int(userId)))

        # Mise à jour
        daoJoueur = DAOJoueurOracle()
        daoJoueur.update(newData)

        # Interface de fin de partie
        #======================================================================================
        uiGameOver = InterfaceGameOver(gameAnalysis)