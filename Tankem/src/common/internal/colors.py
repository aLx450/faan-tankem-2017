# -*- coding: utf-8 -*-

import struct

def hex2rgb(rgb):
	return struct.unpack('BBB', rgb.decode('hex'))

def convertToPandaColors(red, green, blue):
	r = red/255.0
	g = green/255.0
	b = blue/255.0
	return r,g,b