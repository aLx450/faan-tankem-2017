# -*- coding:utf-8 -*-

class DTOGameStats:

	def __init__(self, levelName, usersId, winnerId, shootingStats):
		self.levelName = levelName
		self.usersId = usersId 				# tuple de id
		self.winnerId = winnerId
		self.shootingStats = shootingStats 	# hashtable : idJoueur => {hashtable : weaponName => nbShots}
