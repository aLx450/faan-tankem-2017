# -*- coding: utf-8 -*-
					
from dtoBalance import *

class Sanitizer():


	#Le sanitizer ne possede qu'un flag hasSanitized pour des raisons de tests internes.  
	def __init__(self):
		self.hasSanitized = False

	# Méthode permettant de vérifier si un objet DTO correspond à une valeur nulle
	def verifyDTOEmpty(self, DTOBalanceInstance):
	 	if DTOBalanceInstance is None:
	 		return True
	 	else:
	 		return False



	# Méthode pour développement interne seulement
	# Envoie à la console le statut de toute information manquante en passant outre la vérification des erreurs ailleurs dans le programme
	def sanitizeFlagOnly(self, DTOBalanceInstanceMin, DTOBalanceInstanceMax, DTOBalanceInstanceCurrent):
		DTOBalanceInstanceDefault = DTOBalance()
		if self.verifyDTOEmpty(DTOBalanceInstanceMin) or self.verifyDTOEmpty(DTOBalanceInstanceMax) or self.verifyDTOEmpty(DTOBalanceInstanceCurrent):
			print "Un des objets DTOBalanceInstance du sanitizer est vide."

		else:
			for key in DTOBalanceInstanceCurrent.balance:
				if isinstance(DTOBalanceInstanceCurrent.balance[key], basestring):
					#On deal avec du texte donc on verifie la longueur du String si elle est plus longue que le max
					if len(DTOBalanceInstanceCurrent.balance[key]) > DTOBalanceInstanceMax.balance[key]:
						print "Un des messages dans l'instance DTOBalance courante est plus long que la valeur maximale acceptée : " + key
					
				else:
					#on deal avec des chiffres. Vous êtes un lecteur numérique. 
					if DTOBalanceInstanceCurrent.balance[key] > DTOBalanceInstanceMax.balance[key]:
						print "La variable "+key+" dans l'objet DTOBalance courante est plus haute que la valeur maximale acceptée. "
					
					if DTOBalanceInstanceCurrent.balance[key] < DTOBalanceInstanceMin.balance[key]:
						print "La variable "+key+" dans l'objet DTOBalance courante est plus basse que la valeur minimale acceptée. "

		print "flagging done"
		self.hasSanitized = True

	# Méthode standard utilisée en production 
	# Permet de passer à travers les valeurs d'un objet DTOBalance, peu importe ses modifications en amont 
	# Conçu agnostiquement, il peut épouser des changements dans les quantités et les types de variables
	def sanitizeClean(self, DTOBalanceInstanceMin, DTOBalanceInstanceMax, DTOBalanceInstanceCurrent ):
			# Création d'un objet DTO avec les valeurs par défaut
			DTOBalanceInstanceDefault = DTOBalance()
			# Si l'un ou l'autre des 3 objets DTO nécessaires à la sanitisation est vide on postule un problème de connexion à la BD
			# Ceci cesse automatiquement tout autre filtrage et renvoit le DTO avec les valeurs par défaut
			if (self.verifyDTOEmpty(DTOBalanceInstanceMin) or self.verifyDTOEmpty(DTOBalanceInstanceMax) or self.verifyDTOEmpty(DTOBalanceInstanceCurrent)):		
				DTOBalanceInstanceDefault.balance[DTOBalance.KEY_INTROTEXT] = "Problème de connexion. Configuration par défaut utilisée."
				# Afin de pouvoir être détectés, lus et affichés par les systèmes d'avertissement ultérieurs, les messages
				# d'erreur sont contenus dans l'array "erreurs" du DTOBalance
				DTOBalanceInstanceDefault.erreurs.append("Problème de connexion. Configuration par défaut utilisée.") 
				self.hasSanitized = True
				return DTOBalanceInstanceDefault
			else:
				# Pour chaque couplage clé-valeur, peu importe la clé
				for key in DTOBalanceInstanceCurrent.balance:
					# Vérification si les informations provenant de la bande de données, pour une clé donnée, correspond au type prévu pour celle-ci
					# Si un des deux types n'est pas identique à l'autre, c'est que la base de données renvoit:
					# a) un chiffre alors qu'un string serait attendu pour cette valeur
					# b) un string alors qu'un chiffre serait attendu pour cette valeur
					# La valeur affectée est remplacée par la valeur par défaut.
					if 	(isinstance(DTOBalanceInstanceCurrent.balance[key], basestring)) is not (isinstance(DTOBalanceInstanceDefault.balance[key], basestring)):
						print "Les types de valeurs pour la clé " + key + " sont en conflits. Valeurs par défaut injectees."
						DTOBalanceInstanceCurrent.erreurs.append("Les types de valeurs pour la clé " + key + " sont en conflits. Valeurs par défaut injectees.")
						DTOBalanceInstanceCurrent.balance[key] = DTOBalanceInstanceDefault.balance[key]
					else:
						#Si les types sont identiques, on vérifie si on est en présence de valeurs numériques ou textuelles
						if isinstance(DTOBalanceInstanceCurrent.balance[key], basestring):
							# On traite du texte donc on verifie la longueur du String si elle est plus longue que le max
							if len(DTOBalanceInstanceCurrent.balance[key]) > DTOBalanceInstanceMax.balance[key]:
								print "Un des messages dans l'instance DTOBalance courante est plus long que la valeur maximale acceptée : " + key
								print "Assignation des valeurs par défaut."
								DTOBalanceInstanceCurrent.erreurs.append("Un des messages dans l'instance DTOBalance courante est plus long que la valeur maximale acceptée : " + key + ". Assignation des valeurs par défaut.")
								DTOBalanceInstanceCurrent.balance[key] = DTOBalanceInstanceDefault.balance[key]
						else:
							#On traite avec des chiffres. Vous êtes un lecteur numérique. 
							if DTOBalanceInstanceCurrent.balance[key] > DTOBalanceInstanceMax.balance[key]:
								print "La variable "+key+" dans l'objet DTOBalance courante est plus haute que la valeur maximale acceptée. "
								print "Assignation des valeurs par défaut."
								DTOBalanceInstanceCurrent.erreurs.append("La variable "+key+" dans l'objet DTOBalance courante est plus haute que la valeur maximale acceptée. Assignation des valeurs par défaut.")
								DTOBalanceInstanceCurrent.balance[key] = DTOBalanceInstanceDefault.balance[key]
						
							if DTOBalanceInstanceCurrent.balance[key] < DTOBalanceInstanceMin.balance[key]:
								print "La variable "+key+" dans l'objet DTOBalance courante est plus basse que la valeur minimale acceptée. "
								print "Assignation des valeurs par défaut."
								DTOBalanceInstanceCurrent.erreurs.append("La variable "+key+" dans l'objet DTOBalance courante est plus basse que la valeur minimale acceptée. Assignation des valeurs par défaut." )
								DTOBalanceInstanceCurrent.balance[key] = DTOBalanceInstanceDefault.balance[key]
				self.hasSanitized = True
				return DTOBalanceInstanceCurrent
