# -*- coding:utf-8 -*-

class DTOJoueur:
	KEY_ID = "ID"
	KEY_FIRSTNAME = "FIRSTNAME"
	KEY_LASTNAME = "LASTNAME"
	KEY_USERNAME = "USERNAME"
	KEY_TANK_COLOR = "TANK_COLOR"
	KEY_CALC_NAME = "NOM_CALCULE"
	KEY_EXP = "EXP"
	KEY_STATS = "STATS"
	KEY_LIFE = "LIFE"
	KEY_STRENGTH = "STRENGTH"
	KEY_AGILITY = "AGILITY"
	KEY_DEXTERITY = "DEXTERITY"
	KEY_PLAYER_LEVEL = "PLAYER_LEVEL"
	
	def __init__(self, array):
		self.hashtable = {}
		self.statistiques = {}

		self.hashtable[DTOJoueur.KEY_ID] = array[0][0]
		self.hashtable[DTOJoueur.KEY_FIRSTNAME] = array[0][1]
		self.hashtable[DTOJoueur.KEY_LASTNAME] = array[0][2]
		self.hashtable[DTOJoueur.KEY_USERNAME] = array[0][3]
		self.hashtable[DTOJoueur.KEY_TANK_COLOR] = array[0][4]
		self.hashtable[DTOJoueur.KEY_CALC_NAME] = array[0][5]
		self.hashtable[DTOJoueur.KEY_EXP] = array[0][6]
		self.hashtable[DTOJoueur.KEY_STATS] = self.statistiques
		self.statistiques[DTOJoueur.KEY_LIFE] = array[0][7]
		self.statistiques[DTOJoueur.KEY_STRENGTH] = array[0][8]
		self.statistiques[DTOJoueur.KEY_AGILITY] = array[0][9]
		self.statistiques[DTOJoueur.KEY_DEXTERITY] = array[0][10]
		self.hashtable[DTOJoueur.KEY_PLAYER_LEVEL] = array[0][11]