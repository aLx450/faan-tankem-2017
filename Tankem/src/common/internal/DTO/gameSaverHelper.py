# -*- coding:utf-8 -*-

from .. DAO.daoGameSaver  import *
from saveFrame import *
from dtoSaveGame import *

import threading

# Documentation: 
# Dans map.py, on a une listeTank, listeItem, listeBalles
        # self.listeTank = []
        # self.listeItem = []
        # self.listeBalle = []

# Chaque objet dans ses arrays disposent d'une variable représentant son noeud physique dans l'engine Panda
		# self.noeudPhysique
# Cette variable la devrait pouvoir nous donner son HPR et son POS et probablement son angle par rapport a l'axe nord sud.

class GameSaverHelper:

		def __init__(self):
			self.levelName = "JacquesChirac"
			self.joueursInfos = []
			self.frameArray = []
			self.totalSaves = 0
			self.deduireX = 0
			self.deduireY = 0
			self.mustSave = True
			

		def defineLevelHauteur(self, hauteur):
			self.deduireY = hauteur

		def defineLevelLargeur(self, largeur):
			self.deduireX = largeur

		def ajouterFrame(self, saveFrame):
			self.frameArray.append(saveFrame)
			

		def setGameInfo(self):
			pass

		def finalSave(self, listDTOJoueurInstance):
			self.listeDTOJoueurInstances = listDTOJoueurInstance
			self.DTOTempSave = DTOSaveGame(self.frameArray, self.listeDTOJoueurInstances)
			DAOGameSaverTempInstance = DAOGameSaver()
			DAOGameSaverTempInstance.create(self.DTOTempSave)


		def autoSave(self, listeTankParam, listeBulletParam, listeItemParam):
			currentSaveFrame = SaveFrame()
			
			# On traite les différents arrays reliés aux tanks
			for i in range(len(listeTankParam)):
				
				currentSaveFrame.setTankArrays(listeTankParam[i].noeudPhysique.getHpr(),listeTankParam[i].noeudPhysique.getPos())
				currentSaveFrame.setTankHPArray(listeTankParam[i].pointDeVie)
			# On traite les différents arrays reliés aux balles
			for x in range(len(listeBulletParam)):
				if listeBulletParam[x].etat == 'Detruit':
					pass
				elif listeBulletParam[x].etat == 'actif':
					currentSaveFrame.setActiveBulletArrays(listeBulletParam[x].noeudPhysique.getHpr(),listeBulletParam[x].noeudPhysique.getPos(),listeBulletParam[x].arme,listeBulletParam[x].lanceurId, listeBulletParam[x].balleID)
				elif listeBulletParam[x].etat == 'explose':
					currentSaveFrame.setExplodingBulletArrays(listeBulletParam[x].noeudPhysique.getHpr(),listeBulletParam[x].noeudPhysique.getPos(),listeBulletParam[x].arme,listeBulletParam[x].lanceurId, listeBulletParam[x].balleID)
			
			# On traite les différents arrays reliés aux items
			for y in range(len(listeItemParam)):
				
				if listeItemParam[y].etat == 'detruit':
					pass
				else:
					currentSaveFrame.setItemArrays(listeItemParam[y].noeudPhysique.getPos(), listeItemParam[y].armeId)

			self.frameArray.append(currentSaveFrame)
			self.totalSaves +=1