# -*- coding:utf-8 -*-

class DTOLevel:

	STATUS_ACTIVE = "Actif"
	STATUS_TEST = "Test"
	STATUS_INACTIVE = "Inactif"

	def __init__(self, name, status, date, tiles, playersPosition, minApparitionDelay, maxApparitionDelay):
		self.name = name
		self.status = status
		self.date = date
		self.tiles = tiles
		self.playersPosition = playersPosition
		self.minApparitionDelay = minApparitionDelay
		self.maxApparitionDelay = maxApparitionDelay


class Tile:

	TYPE_FLOOR = "Floor"
	TYPE_FIXED_WALL = "FixedWall"
	TYPE_ANIMATED_WALL_UP = "AnimatedWallUp"
	TYPE_ANIMATED_WALL_DOWN = "AnimatedWallDown"

	def __init__(self, tileType = TYPE_FLOOR, withTree = False):
		self.tileType = tileType
		self.withTree = withTree
