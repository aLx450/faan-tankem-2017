# -*- coding:utf-8 -*-

class SaveFrame:

	def __init__(self):
		self.itemLocationArray = []
		self.itemNameArray = []

		self.activeBulletOrientationArray = []
		self.activeBulletLocationArray = []
		self.activeBulletWeaponNameArray = []
		self.activeBulletPlayerIDArray = []
		self.activeBulletIDArray = []

		self.explodingBulletOrientationArray = []
		self.explodingBulletLocationArray = []
		self.explodingBulletWeaponNameArray = []
		self.explodingBulletPlayerIDArray = []
		self.explodingBulletIDArray = []

		self.tankOrientationArray = []
		self.tankLocationArray = []

		self.tankHP = []

	def setActiveBulletArrays (self, orientationArray, locationArray, weaponName, playerID, bulletID):
		self.activeBulletOrientationArray.append(orientationArray)
		self.activeBulletLocationArray.append(locationArray)
		self.activeBulletWeaponNameArray.append(weaponName)
		self.activeBulletPlayerIDArray.append(playerID)
		self.activeBulletIDArray.append(bulletID)

	def setExplodingBulletArrays (self, orientationArray, locationArray, weaponName, playerID, bulletID):
		self.explodingBulletOrientationArray.append(orientationArray)
		self.explodingBulletLocationArray.append(locationArray)
		self.explodingBulletWeaponNameArray.append(weaponName)
		self.explodingBulletPlayerIDArray.append(playerID)
		self.explodingBulletIDArray.append(bulletID);

	def setTankArrays (self, orientationArray, locationArray):
		self.tankOrientationArray.append(orientationArray)
		self.tankLocationArray.append(locationArray)
	
	def setItemArrays (self, locationArray, nameArray):
		self.itemLocationArray.append(locationArray)
		self.itemNameArray.append(nameArray)

	def setTankHPArray(self, hpArray):
		self.tankHP.append(hpArray)