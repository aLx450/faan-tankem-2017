# -*- coding:utf-8 -*-

class DTOBalance:

	KEY_TERRAINSPEED = "terrainSpeed"
	KEY_TANKSPEED = "tankSpeed"
	KEY_TANKROTATIONSPEED = "tankRotationSpeed"
	KEY_TANKHP = "tankHP"
	KEY_CANNONSPEED = "cannonSpeed"
	KEY_CANNONRELOAD = "cannonReload"
	KEY_SMGSPEED = "smgSpeed"
	KEY_SMGRELOAD = "smgReload"
	KEY_GRENADESPEED = "grenadeSpeed"
	KEY_GRENADERELOAD = "grenadeReload"
	KEY_SHOTGUNSPEED = "shotgunSpeed"
	KEY_SHOTGUNRELOAD = "shotgunReload" 
	KEY_SHOTGUNANGLE = "shotgunAngle"
	KEY_TRAPSPEED = "trapSpeed"
	KEY_TRAPRELOAD = "trapReload"
	KEY_MISSILESPEED = "missileSpeed"
	KEY_MISSILERELOAD = "missileReload"
	KEY_SPRINGHEIGHT = "springHeight"
	KEY_SPRINGRELOAD = "springReload"
	KEY_EXPRADIUS = "expRadius"
	KEY_INTROLENGTH = "introLength"   
	KEY_COUNTDOWNLENGTH = "countdownLength" 
	KEY_INTROTEXT = "introText"
	KEY_STARTTEXT = "startText"
	KEY_ENDTEXT = "overText"
	
	def __init__(self, hashtable = 0):

		if hashtable != 0:
			self.balance = hashtable
			self.erreurs = []
		# if array is not NULL:
		# 	for line in array:
		# 		self.balance[line[0]] = (line[1], line[2], line[3])
		else:
			self.erreurs = []
			self.balance = {}

			#Variables du terrain
			self.balance[DTOBalance.KEY_TERRAINSPEED] = 0.8

			#Variables du tank
			self.balance[DTOBalance.KEY_TANKSPEED] = 7
			self.balance[DTOBalance.KEY_TANKROTATIONSPEED] = 1500
			self.balance[DTOBalance.KEY_TANKHP] = 200

			#Variables du cannon
			self.balance[DTOBalance.KEY_CANNONSPEED] =  14
			self.balance[DTOBalance.KEY_CANNONRELOAD] =  1.2

			#Variables du smg
			self.balance[DTOBalance.KEY_SMGSPEED] =  18
			self.balance[DTOBalance.KEY_SMGRELOAD] = 0.4

			#Variables des grenades
			self.balance[DTOBalance.KEY_GRENADESPEED] = 16
			self.balance[DTOBalance.KEY_GRENADERELOAD] = 0.8

			#Variables du shotgun
			self.balance[DTOBalance.KEY_SHOTGUNSPEED] = 13
			self.balance[DTOBalance.KEY_SHOTGUNRELOAD] =  1.8
			self.balance[DTOBalance.KEY_SHOTGUNANGLE] =  0.4

			#Variables du piege
			self.balance[DTOBalance.KEY_TRAPSPEED] =  1
			self.balance[DTOBalance.KEY_TRAPRELOAD] =  0.8

			#Variables du missile
			self.balance[DTOBalance.KEY_MISSILESPEED] =  30
			self.balance[DTOBalance.KEY_MISSILERELOAD] =  3

			#Variables du spring
			self.balance[DTOBalance.KEY_SPRINGHEIGHT] =  15 #DISCUSSION NECESSAIRE SUR CE LABEL
			self.balance[DTOBalance.KEY_SPRINGRELOAD] =  0.5

			#Variables de l'explosion
			self.balance[DTOBalance.KEY_EXPRADIUS] =  8

			#Variables des timers de messages
			self.balance[DTOBalance.KEY_INTROLENGTH] = 3 #POSSIBLEMENT 0.3. A VOIR
			self.balance[DTOBalance.KEY_COUNTDOWNLENGTH] =  3

			#Variables textuelles
			self.balance[DTOBalance.KEY_INTROTEXT] = "Tankem!"
			self.balance[DTOBalance.KEY_STARTTEXT] = "Appuyer sur F1 pour l'aide"
			self.balance[DTOBalance.KEY_ENDTEXT] = "Joueur (constante du sanitizer) a gagne" #VALEUR TEMPORAIRE	
