# -*- coding:utf-8 -*-

from DTO.dtoJoueur import *

class GameOverAnalysis:
	
	def __init__(self, dtoPlayerList, tankList, winnerDbId):
		self.dtoPlayerList = dtoPlayerList
		self.tankList = tankList
		self.winnerDbId = winnerDbId

	def getExpBonus(self, playerDbId):
		expBonus = 0

		player1 = self.dtoPlayerList[0]
		player2 = self.dtoPlayerList[1]

		# Trouver le favori (level plus élevé que l'autre)
		favoriteDbId = None
		if player1.hashtable[DTOJoueur.KEY_PLAYER_LEVEL] > player2.hashtable[DTOJoueur.KEY_PLAYER_LEVEL]:
			favoriteDbId = player1.hashtable[DTOJoueur.KEY_ID]
		elif player1.hashtable[DTOJoueur.KEY_PLAYER_LEVEL] < player2.hashtable[DTOJoueur.KEY_PLAYER_LEVEL]:
			favoriteDbId = player2.hashtable[DTOJoueur.KEY_ID]

		# Trouver la vie des joueurs { idjoueur : (vieRestante, vieMax) }
		playersLife = {}
		playersLife[player1.hashtable[DTOJoueur.KEY_ID]] = (self.tankList[0].pointDeVie, self.tankList[0].pointDeVieMax)
		playersLife[player2.hashtable[DTOJoueur.KEY_ID]] = (self.tankList[1].pointDeVie, self.tankList[1].pointDeVieMax)

		# Si le joueur est gagnant
		if playerDbId == self.winnerDbId:
			ownRemainingLife = playersLife[playerDbId][0]
			expBonus += 100 + ownRemainingLife * 2
			if favoriteDbId != None and playerDbId != favoriteDbId:
				expBonus += 100
		# Si le joueur est perdant
		else:
			winnerLossedLife = playersLife[self.winnerDbId][1] - playersLife[self.winnerDbId][0]
			expBonus = winnerLossedLife * 2

		return expBonus

	def nextLevelThreshold(self, playerDbId):
		# Déterminer le niveau du joueur
		idInGame = 0 if self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_ID] == playerDbId else 1
		player = self.dtoPlayerList[idInGame]
		level = player.hashtable[DTOJoueur.KEY_PLAYER_LEVEL]

		return self.thresholdFromLevel(level)

	def thresholdFromLevel(self, level):
		threshold = 100 * (level + 1) + 50 * level*level

		return threshold

	def nextLevel(self, playerDbId):
		# Déterminer le niveau et l'exp du joueur
		idInGame = 0 if self.dtoPlayerList[0].hashtable[DTOJoueur.KEY_ID] == playerDbId else 1
		player = self.dtoPlayerList[idInGame]
		exp = player.hashtable[DTOJoueur.KEY_EXP] + self.getExpBonus(player.hashtable[DTOJoueur.KEY_ID])
		currentLevel = player.hashtable[DTOJoueur.KEY_PLAYER_LEVEL]
		# Déterminer le prochain niveau
		nextLevel = currentLevel
		while exp >= self.thresholdFromLevel(nextLevel):
			nextLevel += 1

		return nextLevel
		