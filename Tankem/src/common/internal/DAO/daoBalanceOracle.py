# -*- coding: utf-8 -*-

# Chargement du singleton de connexion
from connection import *

# Chargement des modules DAO et DTO.
from daoBalance import *
from .. DTO.dtoBalance import *

class DAOBalanceOracle(DAOBalance):
	# Fonction de lecture d'une commande à la base de données.
	def executeReadDB(self, command):
		cursor = None
		results = None

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.execute(command) # Execution de la commande passée en paramètre.
				results = cursor.fetchall() # Mettre les résultats de la commande dans un tableau.
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			print("Erreur dans la commande de lecture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return results # Retourne le resultat en format de tableau.

	# Fonction de mise à jour d'une commande à la base de données avec les nouvelles données.
	def executeUpdateDB(self, command, data):
		cursor = None
		commandError = False

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.bindarraysize = 7 # Paramètres de performance de la requête.
				cursor.setinputsizes(float, 150) # Paramètres de performance de la requête.
				cursor.executemany(command, data) # Execution de la commande passée en paramètre avec les différentes données.
				cursor.close() # Fermeture du curseur.
				Connection().getConnectionDB().commit() # Validation de modification dans la base de données.
		except cx_Oracle.DatabaseError as e:
			Connection().getConnectionDB().rollback() # Si il y a durant l'execution on revient en arrière.
			commandError = True
			errorInfo, = e.args
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return commandError

	# Fonction qui permet de rassembler les deux tableaux en un seul tableau.
	def mergeArrays(self, arrayNum, arrayText):
		# Ajout des lignes du tableau de texte dans le tableau numéric.
		for row in arrayText:
			arrayNum.append(row)
		
		return arrayNum # Retour du tableau complet.

	# Fonction qui permet de séparer les données du tableau dans 3 hashtables.
	def splitResults(self, results):
		minHash = {}
		maxHash = {}
		currentHash = {}

		for row in results:
			minHash[row[0]] = row[1]
			maxHash[row[0]] = row[2]
			currentHash[row[0]] = row[3]
		
		return (minHash, maxHash, currentHash) # Retour d'un tuple des 3 hashtables.

	# Fonction qui permet de séparer le DTO en deux tableaux pour les valeurs numérics et textuelles.
	def splitDtoToArraysTuple(self, dtoBalance):
		arrayTupleNum = []
		arrayTupleText = []
		for key, value in dtoBalance.balance.iteritems(): # Parcourir la hashtable du DTO.
			data = (value, key)
			# Si la valeur est numerique
			try:
				val = float(value)
				arrayTupleNum.append(data)
			# Si la valeur est textuelle
			except ValueError:
				arrayTupleText.append(data)
			
		return (arrayTupleNum, arrayTupleText)
##################################################################################################

	def create(self):
		pass

	# Fonction qui permet de faire la lecture des informations de la base de données.
	def read(self):
		commandNum = "SELECT PROPERTY_NAME, MIN_VALUE, MAX_VALUE, CURRENT_VALUE FROM NUMERIC_VALUES"
		commandText = "SELECT PROPERTY_NAME, MIN_LENGTH, MAX_LENGTH, CURRENT_VALUE FROM TEXT_VALUES"

		# Lecture des informations dans la base de données pour les données numéric.
		resultsNum = self.executeReadDB(commandNum)

		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			resultsText = self.executeReadDB(commandText)
			results = self.mergeArrays(resultsNum, resultsText) # Assemblage des deux tableaux.

			hashtables = self.splitResults(results) # Séparation du résultat en trois DTO.

			return (DTOBalance(hashtables[0]), DTOBalance(hashtables[1]), DTOBalance(hashtables[2])) # On retourne les trois DTO.
		else:
			return (None, None, None) # Il y a eu une erreur de connection donc je retourne trois None.

	# Fonction qui permet de changer les valeurs dans la base de données via le DTO propre.
	def update(self, dtoBalance):
		commandError = False
		commandNumericValues = "UPDATE NUMERIC_VALUES SET CURRENT_VALUE = :1 WHERE PROPERTY_NAME = :2"
		commandTextValues = "UPDATE TEXT_VALUES SET CURRENT_VALUE = :1 WHERE PROPERTY_NAME = :2"
		
		dataNum, dataText = self.splitDtoToArraysTuple(dtoBalance) # Séparation du DTO en deux tableaux.
		commandError = self.executeUpdateDB(commandNumericValues, dataNum) # Mise à jour de la table numéric.

		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			if commandError is not True: # S"il y a pas d'erreur dans la dernière commande.
				commandError = self.executeUpdateDB(commandTextValues, dataText) # Mise à jour de la table textuelle.

		return (Connection().getErrorConnectionDB(), commandError) # Permet de connaitre l'état de la mise à jour.

	def delete(self):
		pass