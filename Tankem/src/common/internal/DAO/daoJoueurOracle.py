# -*- coding: utf-8 -*-

# Chargement du singleton de connexion
from connection import *

# Chargement du DTO.
from .. DTO.dtoJoueur import *
import bcrypt

class DAOJoueurOracle():
	def executeReadDB(self, command, data):
		cursor = None
		results = None

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.bindarraysize = 7 # Paramètres de performance de la requête.
				cursor.setinputsizes(150) # Paramètres de performance de la requête.
				cursor.execute(command, data) # Execution de la commande passée en paramètre avec les différentes données.
				results = cursor.fetchall()
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return results

	# Fonction de mise à jour d'une commande à la base de données avec les nouvelles données.
	def executeUpdateDB(self, command, data, cursor):
		commandError = False

		try:
			cursor.bindarraysize = 7 # Paramètres de performance de la requête.
			cursor.executemany(command, data)
			cursor.close()
			Connection().getConnectionDB().commit()
		except cx_Oracle.DatabaseError as e:
			Connection().getConnectionDB().rollback() # Si il y a erreur durant l'execution on revient en arrière.
			commandError = True
			errorInfo, = e.args
			Connection().setErrorMessageDB("Erreur dans la commande d'écriture !\n")
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return commandError


	def create(self):
		pass
	
	def read(self, username):
		command = "SELECT ID, FIRSTNAME, LASTNAME, USERNAME, TANK_COLOR, NOM_CALCULE, EXP, LIFE, STRENGTH, AGILITY, DEXTERITY, PLAYER_LEVEL FROM USERS WHERE USERNAME = :1"
		data = [username,]

		results = self.executeReadDB(command, data)

		if results is not None: # S'il n'y a pas d'erreur de connection.
			return DTOJoueur(results) # On retourne le DTO
		else:
			return None # Il y a eu une erreur de connection donc je retourne None
		
	
	def update(self, expLevelIdTuples):
		commandError = False

		# Commande Oracle
		command = "UPDATE USERS SET EXP = :1, PLAYER_LEVEL = :2 WHERE ID = :3"

		# Exécution des commandes Oracle
		if Connection().getErrorConnectionDB() is not True:
			cursor = Connection().getConnectionDB().cursor()
			cursor.setinputsizes(int, int, int)
			commandError = self.executeUpdateDB(command, expLevelIdTuples, cursor)

			if commandError is not True:
				Connection().setErrorMessageDB("")

		return (Connection().getErrorConnectionDB(), commandError) # Permet de connaitre l'état de la mise à jour.

	def delete(self):
		pass

	def validLogin(self, infoUser):
		success = False
		userFound = False
		command = "SELECT PASSWORD FROM USERS WHERE USERNAME = :1"
		data = [infoUser[0],]

		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			results = self.executeReadDB(command, data)
			if not results:
				return (success, None, Connection().getErrorConnectionDB(), userFound)
			else:			
				userFound = True
				if bcrypt.checkpw(infoUser[1], results[0][0]):
					success = True
					return (success, self.read(infoUser[0]), Connection().getErrorConnectionDB(), userFound)
				else:
					return (success, None, Connection().getErrorConnectionDB(), userFound)
		else:
			return (success, None, Connection().getErrorConnectionDB(), userFound) # Il y a eu une erreur de connection donc je retourne None