# -*- coding: utf-8 -*-

# Chargement du singleton de connexion
from connection import *

# Chargement du module DTO.
from .. DTO.dtoGameStats import *


class DAOGameStatsOracle():
	def __init__(self):
		self.lastGameId = None

	# Fonction de lecture d'une commande à la base de données.
	def executeReadDB(self, command):
		cursor = None
		results = None

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.execute(command)
				results = cursor.fetchall()
				cursor.close()
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			Connection().setErrorMessageDB("Erreur dans la commande de lecture !\n")
			print("Erreur dans la commande de lecture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return results # Retourne le resultat en format de tableau.

	# Fonction de mise à jour d'une commande à la base de données avec les nouvelles données.
	def executeUpdateDB(self, command, data, cursor):
		commandError = False

		try:
			cursor.bindarraysize = 7 # Paramètres de performance de la requête.
			cursor.executemany(command, data)
			cursor.close()
			Connection().getConnectionDB().commit()
		except cx_Oracle.DatabaseError as e:
			Connection().getConnectionDB().rollback() # Si il y a erreur durant l'execution on revient en arrière.
			commandError = True
			errorInfo, = e.args
			Connection().setErrorMessageDB("Erreur dans la commande d'écriture !")
			# Contrainte de clé unique pour l'insertion des armes non tenu en compte
			if (not(command == "INSERT INTO WEAPONS (NAME) VALUES (:1)" and "ORA-00001" in errorInfo.message)):
				print("Erreur dans la commande d'écriture !")
				print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
				print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
				print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return commandError


##################################################################################################


	def getGameStatsArrays(self, dtoGameStats, lastGameRowId):
		# Partie
		weapons = []
		shootingStats = []

		for userId, weaponStats in dtoGameStats.shootingStats.iteritems():
			for weaponName, nbShots in weaponStats.iteritems():
				# Armes
				if (weaponName,) not in weapons:
					weapons.append((weaponName,))
				# Nb tirs
				shootingStats.append((int(lastGameRowId), int(userId),str(weaponName), int(nbShots)))

		return (weapons, shootingStats)


##################################################################################################



	def create(self):
		pass

	def read(self):
		pass

	def getLastGameId(self):
		if self.lastGameId == None:
			commandGameId = "select ID from GAMES where ID=(select max(ID) from GAMES)"
			self.lastGameId = self.executeReadDB(commandGameId)[0][0]
		return self.lastGameId

	def update(self, dtoGameStats):
		commandError = False
		game = None
		weapons = []
		shootingStats = []

		# Commandes Oracle
		commandGame = "INSERT INTO GAMES (LEVEL_NAME, PLAYER1, PLAYER2, WINNER) VALUES (:1, :2, :3, :4)"
		commandWeapons = "INSERT INTO WEAPONS (NAME) VALUES (:1)"
		commandShootingStats = "INSERT INTO SHOOTING_STATS (GAME_ID, USER_ID, WEAPON_NAME, NB_SHOTS) VALUES (:1, :2, :3, :4)"

		# Exécution des commandes Oracle
		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			cursor = Connection().getConnectionDB().cursor()
			cursor.setinputsizes(20, int, int, int)
			commandGameError = self.executeUpdateDB(
				commandGame, [(dtoGameStats.levelName, dtoGameStats.usersId[0], dtoGameStats.usersId[1], dtoGameStats.winnerId),], cursor
			)
			# Incrémenter le ID pour être équivalent à la BD
			if commandGameError == "" and self.lastGameId != None:
				self.lastGameId += 1
			# Chercher le dernier ID dans la table Games
			lastGameRowId = self.getLastGameId()
			# Convertir le DTO en arrays
			weapons, shootingStats = self.getGameStatsArrays(dtoGameStats, lastGameRowId)
		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			cursor = Connection().getConnectionDB().cursor()
			cursor.setinputsizes(50)
			commandWeaponsError = self.executeUpdateDB(commandWeapons, weapons, cursor)
		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			cursor = Connection().getConnectionDB().cursor()
			cursor.setinputsizes(int, int, 50, int)
			commandShootingStatsError = self.executeUpdateDB(commandShootingStats, shootingStats, cursor)

		# Erreurs (On ne tient pas compte de l'erreur de violation de contraite unique pour la table Weapons)
		commandError = commandGameError or commandShootingStatsError

		return (Connection().getErrorConnectionDB(), commandError) # Permet de connaitre l'état de la mise à jour.

	def delete(self):
		pass