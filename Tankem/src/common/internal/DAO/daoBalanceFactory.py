# -*- coding:utf-8 -*-

from daoBalanceOracle import *
from daoBalanceCSV import *

class DAOBalanceFactory:

	def create(self, specialisation):
		if specialisation == "csv":
			return DAOBalanceCSV()
		elif specialisation == "oracle":
			return DAOBalanceOracle()