# -*- coding:utf-8 -*-

from abc import ABCMeta, abstractmethod

class DAOBalance():
	__metaclass__ = ABCMeta

	@abstractmethod
	def create(self):
		pass

	@abstractmethod
	def read(self):
		pass

	@abstractmethod
	def update(self, dtoBalances):
		pass

	@abstractmethod
	def delete(self):
		pass