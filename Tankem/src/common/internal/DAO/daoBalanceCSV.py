# -*- coding:utf-8 -*-

import os
import csv
import codecs
import errno
from Tkinter import Tk
from tkFileDialog import *
from daoBalance import *
from .. DTO.dtoBalance import *
from .. DTO.sanitizer import *

class DAOBalanceCSV(DAOBalance):

	FILE_NAME = "BalanceTankem.csv"

	def __init__(self):
		self.fileName = ""

	def chooseFile(self, create = False):
		# Enlever la fenetre principale (canevas)
		Tk().withdraw()
		desktopPath = os.path.expanduser('~') + "/Desktop"
		try:
			if create:
				self.fileName = asksaveasfilename(filetypes=[("CSV Files", "*.csv"), ("All Files","*")], defaultextension="*.csv", initialfile=self.fileName, initialdir=desktopPath)
			else:
				self.fileName = askopenfilename(filetypes=[("CSV Files", "*.csv"), ("All Files","*")], defaultextension="*.csv", initialfile=self.fileName, initialdir=desktopPath)
		except OSError as e:
			print e.errno
		return self.fileName

	def create(self):
		pass
		

	def read(self, fileName = FILE_NAME):
		htMin = {}
		htMax = {}
		htCurrent = {}
		if (self.fileName != ""):	
			with open(self.fileName) as csvfile:
				reader = csv.DictReader(csvfile)
				for row in reader:
					key = row['key']
					minVal = row['min']
					maxVal = row['max']
					currentVal = row['current']
					try:
						minVal = float(row['min'])
					except:
						minVal = row['min']
					try:
						maxVal = float(row['max'])
					except:
						maxVal = row['max']
					try:
						currentVal = float(row['current'])
					except:
						currentVal = row['current']
					htMin[key] = minVal
					htMax[key] = maxVal
					htCurrent[key] = currentVal
				dtoMin = DTOBalance(htMin)
				dtoMax = DTOBalance(htMax)
				dtoCurrent = DTOBalance(htCurrent)
				sanitizer = Sanitizer()
				dtoClean = sanitizer.sanitizeClean(dtoMin, dtoMax, dtoCurrent)
				return dtoClean
				

	def update(self, dtoBalances):
		dtoMin = dtoBalances[0]
		dtoMax = dtoBalances[1]
		sanitizer = Sanitizer()
		dtoCurrentClean = sanitizer.sanitizeClean(dtoBalances[0], dtoBalances[1], dtoBalances[2])

		if (self.fileName != ""):
			with open(self.fileName, 'wb') as csvfile:
				fieldnames = ['key', 'min', 'max', 'current']
				writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

				writer.writeheader()
				for key, minVal in dtoMin.balance.iteritems():
					maxVal = dtoMax.balance[key]
					currentVal = dtoCurrentClean.balance[key]
					writer.writerow({'key': key, 'min': minVal, 'max': maxVal, 'current': currentVal})


	def delete(self):
		pass