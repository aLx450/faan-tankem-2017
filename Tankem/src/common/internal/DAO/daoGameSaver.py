# -*- coding: utf-8 -*-

# Chargement du singleton de connexion
from connection import *

class DAOGameSaver: 
	def __init__(self):
		self.saveIdActuel = None
		self.gameIdActuel = None

		self.commandTankHP = None
		self.dataTank = []

		self.commandItem = None
		self.dataItem = []

		self.commandBullets = None
		self.dataBullets = []

		self.dataSaveID = []

		self.dataGame = []

		self.frameSkip = 1
		self.mustSave = False
		self.commandGameRes = ""
		self.resData = []

	def executeReadDB(self, command, data):
		
		cursor = None
		results = None

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.bindarraysize = 7 # Paramètres de performance de la requête.
				cursor.setinputsizes(150) # Paramètres de performance de la requête.
				cursor.execute(command, data) # Execution de la commande passée en paramètre avec les différentes données.
				results = cursor.fetchall()
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return results

	def executeUpdateDB(self, command, data, label):
		cursor = None
		commandError = False
		lastRowId = None

		try:
			if Connection().getErrorConnectionDB() is not True:
				cursor = Connection().getConnectionDB().cursor()
				
				if label == "tank":
					cursor.bindarraysize = 11 # Paramètres de performance de la requête.
					cursor.setinputsizes(float, float, float, float, float, float, float, float, float, float, 25) # Paramètres de performance de la requête.
				elif label == "item":
					cursor.bindarraysize = 5 # Paramètres de performance de la requête.
					cursor.setinputsizes(float, 25, float, float, float) # Paramètres de performance de la requête.
				elif label == "active":
					cursor.bindarraysize = 7 # Paramètres de performance de la requête.
					cursor.setinputsizes(float, float, float, float, float, 25, float, float) # Paramètres de performance de la requête.
				elif label == "saveID":
					cursor.bindarraysize = 1 # Paramètres de performance de la requête.
					cursor.setinputsizes(float) # Paramètres de performance de la requête.
				elif label == "gameData":
					cursor.bindarraysize = 10 # Paramètres de performance de la requête.
					cursor.setinputsizes(float, float, 25, 25, float, float, float, float, 25, 25) # Paramètres de performance de la requête.
				elif label == "resolution":
					cursor.bindarraysize = 2 # Paramètres de performance de la requête.
					cursor.setinputsizes(float, float) # Paramètres de performance de la requête.
				cursor.executemany(command, data)
				
				cursor.close()
				Connection().getConnectionDB().commit()
				
		except cx_Oracle.DatabaseError as e:
			Connection().getConnectionDB().rollback() # Si il y a erreur durant l'execution on revient en arrière.
			commandError = True
			errorInfo, = e.args
			Connection().setErrorMessageDB("Erreur dans la commande d'écriture !\n")
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return (commandError)

	def executeReadCurrentState(self):
		
		cursor = None
		results = None
		command = 'select max(saveID) from gamesave'

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				
				cursor.execute(command) # Execution de la commande passée en paramètre avec les différentes données.
				results = cursor.fetchone()[0]
				
				Connection().getConnectionDB().commit()
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante
		return results

	def executeReadCurrentStateGame(self):
		
		cursor = None
		results = None
		command = 'select max(ID) from games'

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				
				cursor.execute(command) # Execution de la commande passée en paramètre avec les différentes données.
				results = cursor.fetchone()[0]
				
				Connection().getConnectionDB().commit()
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			print("Erreur dans la commande d'écriture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante
		return results

	def createGameData(self, listeJoueursInfos, levelName):
		for person in listeJoueursInfos:
			self.nomPlayer = person.hashtable['FIRSTNAME'] + " " + person.hashtable['LASTNAME']
			self.dataGame.append([self.gameIdActuel, person.hashtable['ID'], person.hashtable['TANK_COLOR'], levelName, person.statistiques['LIFE'], person.statistiques['STRENGTH'], person.statistiques['AGILITY'], person.statistiques['DEXTERITY'],  person.hashtable['NOM_CALCULE'], self.nomPlayer ])
			

	def create(self, DTOSaveGame):
		
		self.saveIdActuel = self.executeReadCurrentState()
		if self.saveIdActuel == None:
			self.saveIdActuel = 0
		# ALLER QUERRY LA BD A NICO POUR SON GAMEID
		self.gameIdActuel = self.executeReadCurrentStateGame()
		if self.gameIdActuel == None:
			self.gameIdActuel = 0
		self.gameIdActuel +=1
		self.joueursId= DTOSaveGame.joueursId

		self.createGameData(DTOSaveGame.joueursInfo, DTOSaveGame.levelName)
		self.commandGameData = "INSERT INTO PLAYERSAVE (GameID, JoueurID, TankCouleur, Level_Name, Vie, Force, Agilite, Dexterite, NomCompose, UserName) VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10)"
		self.executeUpdateDB(self.commandGameData, self.dataGame, "gameData")


		
		# CALCUL de la resolution
		self.nbFrames = len(DTOSaveGame.frameArray)
		if self.nbFrames <= 600:
			self.mustSave = True
		elif self.nbFrames > 600 and self.nbFrames <= 1200:
			self.mustSave = True
			self.frameSkip = 2
		elif self.nbFrames >1200:
			self.frameSkip =3 

		self.commandGameRes = "INSERT INTO GAMERES (GameID, FrameSkip) VALUES (:1, :2)"
		self.resData.append([self.gameIdActuel, self.frameSkip])
		self.executeUpdateDB(self.commandGameRes, self.resData, "resolution")

		self.tempFrameCount = 0
		for frame in DTOSaveGame.frameArray:
			if self.tempFrameCount%self.frameSkip == 0: 
				self.dataSaveID.append([self.gameIdActuel])
				self.saveIdActuel +=1
				

				self.createActiveBullets(frame)
				self.createExplodingBullets(frame)
				self.createItems(frame)
				self.createTank(frame)
			self.tempFrameCount +=1
			
		self.commandSaveID = "INSERT INTO GAMESAVE (gameID) VALUES (:1)"
		self.commandTankHP = "INSERT INTO TANKSAVE (saveID, joueurID, tankX, tankY, tankZ, tankH, tankP, tankR, tankHP, tankHasShot, tankWeapon) VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11)"
		self.commandItem =  "INSERT INTO ITEMSAVE (saveID, armeName, armeX, armeY, armeZ) VALUES (:1, :2, :3, :4, :5)"
		self.commandBullets =  "INSERT INTO BULLETSAVE (saveID, balleX, balleY, balleZ, balleActive, weaponName, ingameID, shooterID) VALUES (:1, :2, :3, :4, :5, :6, :7, :8)"	

		self.executeUpdateDB(self.commandSaveID, self.dataSaveID, "saveID")
		self.executeUpdateDB(self.commandTankHP, self.dataTank, "tank")	
		self.executeUpdateDB(self.commandItem, self.dataItem, "item")
		self.executeUpdateDB(self.commandBullets, self.dataBullets, "active")
			
	def createTank(self, frame):
		if (not frame.tankOrientationArray) or (not frame.tankLocationArray) or (not frame.tankHP):
			pass
		else:
			self.nbTankIndex = 0
			for tankHPIndex in frame.tankHP:
				self.dataTank.append( [self.saveIdActuel, self.joueursId[self.nbTankIndex], frame.tankLocationArray[self.nbTankIndex][0], frame.tankLocationArray[self.nbTankIndex][1], frame.tankLocationArray[self.nbTankIndex][2],frame.tankOrientationArray[self.nbTankIndex][0],frame.tankOrientationArray[self.nbTankIndex][1],frame.tankOrientationArray[self.nbTankIndex][2], tankHPIndex, 0, 'Canon' ] )
				self.nbTankIndex = 1
				# CHANGER 6 pour un joueurID valide dynamique.


	def createItems(self, frame):
		if (not frame.itemLocationArray) or (not frame.itemNameArray):
			pass
		else:
			self.commandItem =  "INSERT INTO ITEMSAVE (saveID, armeName, armeX, armeY, armeZ) VALUES (:1, :2, :3, :4, :5)"
			self.nbItemIndex = 0
			for itemIndex in frame.itemLocationArray:			
				self.dataItem.append( [self.saveIdActuel, frame.itemNameArray[self.nbItemIndex], itemIndex[0], itemIndex[1], itemIndex[2]])
				self.nbItemIndex +=1
				

	def createActiveBullets(self, frame):
		if not frame.activeBulletLocationArray:
			pass
		else:
			self.currentActiveFrameIndex = 0
			for bulletIndex in frame.activeBulletLocationArray:
				joueurIDTempo = self.joueursId[frame.activeBulletPlayerIDArray[self.currentActiveFrameIndex]] #ICI CEST PAS BULLET INDEX $
				weaponNameTempo = frame.activeBulletWeaponNameArray[self.currentActiveFrameIndex]
				self.dataBullets.append([self.saveIdActuel, bulletIndex[0], bulletIndex[1], bulletIndex[2], 1, weaponNameTempo,  frame.activeBulletIDArray[self.currentActiveFrameIndex], joueurIDTempo])
				self.currentActiveFrameIndex+=1
			self.currentActiveFrameIndex = 0
	
	def createExplodingBullets(self, frame):
		if not frame.explodingBulletLocationArray:
			pass
		else:
			self.currentActiveFrameIndex = 0
			for bulletIndex in frame.explodingBulletLocationArray:
				joueurIDTempo = self.joueursId[frame.explodingBulletPlayerIDArray[self.currentActiveFrameIndex]] #ICI CEST PAS BULLET INDEX $
				weaponNameTempo = frame.explodingBulletWeaponNameArray[self.currentActiveFrameIndex]
				self.dataBullets.append([self.saveIdActuel, bulletIndex[0], bulletIndex[1], bulletIndex[2], 0, weaponNameTempo, frame.explodingBulletIDArray[self.currentActiveFrameIndex], joueurIDTempo])
				self.currentActiveFrameIndex+=1
			self.currentActiveFrameIndex = 0	
			

	def read(self, username):
		command = "SELECT ID, FIRSTNAME, LASTNAME, USERNAME, TANK_COLOR, EXP, LIFE, STRENGTH, AGILITY, DEXTERITY, PLAYER_LEVEL FROM USERS WHERE USERNAME = :1"

		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
			results = self.executeReadDB(command, data)
			return DTOJoueur(results) # On retourne le DTO
		else:
			return None # Il y a eu une erreur de connection donc je retourne None
			
	def update(self):
		pass

	def delete(self):
		pass