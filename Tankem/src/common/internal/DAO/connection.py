# -*- coding: utf-8 -*-

# Mot de passe de la base de données.
password = 'FAANb62'

# Module Python pour interagir avec une BD Oracle se nomme: CxOracle.
import cx_Oracle

class Connection():

	# Création d'une variable "static"
	__share_state = {}

	def __init__(self):
		# Le dictionnaire d'attribut sera le même pour toutes les instances
		self.__dict__ = self.__share_state

		# Attention! On ne doit pas écrasser les attributs déjà existant car on repasse constament dans ce code.
		if not hasattr(self, "errorConnection"):
			self.errorConnection = False

		if not hasattr(self, "errorMessage"):
			self.errorMessage = ""

		if not hasattr(self, "connection"):
			try: # Test de connection.
				self.connection = cx_Oracle.connect('e1461027', password, '10.57.4.60/DECINFO.edu')
				self.errorConnection = False # La connexion est fonctionnelle.
			except cx_Oracle.DatabaseError as e:
				errorInfo, = e.args
				self.connection = None # Connection initialisée à None
				self.errorConnection = True # Une erreur est arrivée durant la tentative de connection.
				self.errorMessage += "Erreur dans la commande de connection à la base de données !\n"
				print("Erreur dans la commande de connection à la base de données !")
				print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
				print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
				print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

	def getConnectionDB(self):
		return self.connection

	def getErrorConnectionDB(self):
		return self.errorConnection

	def getErrorMessageDB(self):
		return self.errorMessage

	def setErrorMessageDB(self, message):
		self.errorMessage += message

	# Fonction de fermeture de la connection à la base de données.
	def closeConnectionDB(self):
		# Si jamais il y a une erreur de connection il n'y a pas de fermeture de connection à faire.
		if self.errorConnection is not True:
			self.connection.close()