# -*- coding: utf-8 -*-

# Chargement du singleton de connexion
from connection import *

# Chargement des modulesDTO.
from .. DTO.dtoLevel import *


class DAOLevelOracle():
	# Fonction de lecture d'une commande à la base de données.
	def executeReadDB(self, command):
		cursor = None
		results = None

		try:
			if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.
				cursor = Connection().getConnectionDB().cursor() # Création du curseur sur la connection.
				cursor.execute(command) # Execution de la commande passée en paramètre.
				results = cursor.fetchall() # Mettre les résultats de la commande dans un tableau.
				cursor.close() # Fermeture du curseur.
		except cx_Oracle.DatabaseError as e:
			errorInfo, = e.args
			Connection().setErrorMessageDB("Erreur dans la commande de lecture !\n")
			print("Erreur dans la commande de lecture !")
			print(errorInfo.code) # Code de l'erreur du style ORA-12541 pratique pour trouver l'info sur internet
			print(errorInfo.message) # Détail de l'erreur, mais souvent obscure malheureusement
			print(errorInfo.context) # Donne le nom de la fonction Oracle qui plante

		return results # Retourne le resultat en format de tableau.

##################################################################################################

	def create(self):
		pass

	# Fonction qui permet de faire la lecture des informations de la base de données.
	def read(self):
		htDTOLevel = {}

		# Query SQL
		commandLevels = "SELECT NAME, STATUS, WIDTH, HEIGHT, MIN_APPARITION_DELAY, MAX_APPARITION_DELAY, CREATION_DATE FROM LEVELS"
		commandTiles = "SELECT LEVEL_NAME, POS_X, POS_Y, TILE_TYPE, WITH_TREE FROM TILES"
		commandPlayersPosition = "SELECT LEVEL_NAME, PLAYER_NO, POS_X, POS_Y FROM PLAYERS_POSITION"

		# Lecture des informations dans la base de données
		resultLevels = self.executeReadDB(commandLevels)

		if resultLevels is not None:
			resultTiles = self.executeReadDB(commandTiles)
		
		if resultTiles is not None:
			resultPlayersPosition = self.executeReadDB(commandPlayersPosition)

		# Créer un DTOLevel par niveau et remplir ses données
		# row[0] correspond toujours au nom du niveau (utilisé comme clé pour htDTOLevel)
		if Connection().getErrorConnectionDB() is not True: # S'il n'y a pas d'erreur de connection.	
			# Création des niveaux	
			for row in resultLevels:
				tableName = row[0]
				status = row[1]
				date = row[6]
				minDelay = row[4]
				maxDelay = row[5]
				width = row[2]
				height = row[3]
				# initialisation avec tuiles vides pour pouvoir modifier le tableau sans append()
				tiles = []
				for y in range(height):
					tiles.append([])
					for x in range(width):
						tiles[y].append(Tile())
				playersPosition = {}
				htDTOLevel[tableName] = DTOLevel(tableName, status, date, tiles, playersPosition, minDelay, maxDelay)
				i = 0
				
			# Création des tuiles
			for row in resultTiles:
				tableName = row[0]
				withTree = True if row[4] == 'Y' else False
				x = row[1]
				y = row[2]
				tileType = row[3]
				htDTOLevel[tableName].tiles[y][x] = Tile(tileType, withTree)

			# Création des positions de joueur
			for row in resultPlayersPosition:
				tableName = row[0]
				playerNo = row[1]
				x = row[2]
				y = row[3]
				htDTOLevel[tableName].playersPosition[playerNo] = (x, y)

			
			# Trie des dto en ordre alphabétique
			dtoLevels = []
			for key in sorted(htDTOLevel):
 				dtoLevels.append(htDTOLevel[key])

			# Retour de tous les dto et des erreurs
			return (dtoLevels, Connection().getErrorMessageDB())

		# Si erreur de connexion
		else:
			return ([], Connection().getErrorMessageDB())


	def update(self, dtoBalance):
		pass

	def delete(self):
		pass