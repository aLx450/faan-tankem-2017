 ----------------------------
| Projet Tankem | Phase 1
 ----------------------------
 Fait part
 Flavio Milicia
 Alexandre Garneau
 Alexandre Daigneault
 Nicola Demers

 ----------------------------
| Pour lancer Tankem
 ----------------------------
 - Double-cliquez sur le fichier startTankem.cmd localis� a la racine du projet

 ----------------------------
| Pour lancer l'�diteur de Niveaux
 ----------------------------
 - * Prenez en compte que la configuration PHP est bas�e sur une structure de dossiers propre � chaque utilisateur 
     Assurez-vous que votre propre fichier apacheExt.conf est � la racine du projet

 - Double-cliquez sur le fichier startLevelEditor.cmd localis� a la racine du projet
 - Lorsque la fen�tre de commande vous demande de red�marrer votre module Apache, faites-le.
   Les fichiers de configuration sont automatiquement d�pla��s au bon endroit.
 - Appuyez sur Entr�e pour fermer la fen�tre

 ----------------------------
| Pour ex�cuter les scripts de cr�ation de toutes les tables n�cessaires au projet
 ----------------------------
 * Id�alement, faites cette �tape avant votre premi�re partie

 - A partir de la racine du projet, d�placez-vous dans le dossier tools
 - Double cliquez sur le fichier dropAllTables.cmd pour s'assurer de repartir � neuf
 - Double-cliquez sur le fichier createAndPopulateAllTables.cmd pour cr�er et populer toutes les tables
 - Appuyez sur Entr�e pour fermer les fen�tres.

 ----------------------------
| Pour r�cup�rer les donn�es de balance contenues dans la base de donn�es
 ----------------------------
 - A partir de la racine du projet, d�pla�ez-vous dans tools/ChangerBalance
 - Double-cliquez sur le fichier lecture_balance.cmd
 - S�lectionnez l'emplacement et le nom de fichier qui va contenir les donn�es charg�es.
 - Les donn�es sont charg�es

 ----------------------------
| Pour sauvegarder les donn�es de balance pr�sentes dans fichier CSV
 ----------------------------
 - A partir de la racine du projet, d�pla�ez-vous dans tools/ChangerBalance
 - Double-cliquez sur le fichier ecriture_balance.cmd
 - S�lectionnez le fichier .CSV � partir duquel vous voulez que le donn�es soient charg�es
 - Les donn�es sont enregistr�es dans la base de donn�es et accessible par le jeu